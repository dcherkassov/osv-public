#!/bin/bash

JEN_BACKEND=/home/jenkins
JEN_FRONTEND=/var/lib/jenkins

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -o verbose

echo Installing prereqs...
echo deb http://ftp.us.debian.org/debian wheezy main non-free >> /etc/apt/sources.list
aptitude update
aptitude -VR install daemon gcc make python-paramiko python-lxml python-simplejson python-matplotlib libtool xmlstarlet autoconf automake rsync openjdk-7-jre openjdk-7-jdk iperf netperf netpipe-tcp texlive-latex-base sshpass

echo "adding multiarch support"
dpkg --add-architecture i386
aptitude update
aptitude install -VR ia32-libs

echo Please select NO in the following dialogue. Press return now.
read
dpkg-reconfigure dash

echo Downloading and installing jenkins pkg...
# wget http://ftp.vim.org/programming/jenkins/debian-stable/jenkins_1.509.2_all.deb
dpkg -i jenkins_1.509.2_all.deb

echo Installing stuff ...
mkdir $JEN_BACKEND
cp -ar backend/* $JEN_BACKEND/
cp -ar frontend/* $JEN_FRONTEND/
cp jenkins.cfg /etc/default/jenkins

ln -fns $JEN_BACKEND/logs $JEN_FRONTEND/userContent/osv.logs

mkdir $JEN_BACKEND/work

wget "http://downloads.sourceforge.net/project/getfo/texml/texml-2.0.2/texml-2.0.2.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fgetfo%2F&ts=1398789654&use_mirror=sunet" -O texml.tar.gz
tar xvf texml.tar.gz
cd texml-2.0.2
python setup.py install
cd -


echo "fetching tools"
git clone https://4da@bitbucket.org/4da/osv-tools-public.git tools
mkdir $JEN_BACKEND/tools

tools/osv-eglibc-x86_64-meta-toolchain-cortexa15hf-vfp-neon-toolchain-1.5+snapshot.sh -y -d /home/jenkins/tools/renesas-arm

tools/osv-eglibc-x86_64-meta-toolchain-core2-32-toolchain-1.5+snapshot.sh -y -d /home/jenkins/tools/intel-minnow

chown -R jenkins  $JEN_BACKEND
chown -R jenkins  $JEN_FRONTEND
chown -R jenkins  /var/cache/jenkins
chown jenkins /etc/default/jenkins

mkdir /home/jenkins/overlays/work
chown jenkins /home/jenkins/overlays

mkdir /home/jenkins/logs/logruns
chown jenkins /home/jenkins/logs/logruns

cp jenkins.cfg /etc/default/jenkins

echo Startings jenkins...
/etc/init.d/jenkins start

echo Installing custom frontend...
cd jenkins-updates
./updates.sh

