tarball=signaltest.tar.gz

function test_build {
  make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
	put signaltest  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_SIGNALTEST_LOOPS

	report "cd $OSV_HOME/osv.$TESTDIR; ./signaltest  -l $BENCHMARK_SIGNALTEST_LOOPS -q"  
}

. ../scripts/benchmark.sh
