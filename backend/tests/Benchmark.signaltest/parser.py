#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys
sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d_ .]+.[gle]{2}\]"
cur_search_pat = re.compile("^T:([\s\d]+)(.*)P:(.*)C:(.*)Min:([\s\d]+)Act:([\s\d]+)Avg:([\s\d]+)Max:([\s\d]+)",re.MULTILINE)

res_dict = {}
cur_dict = {}

pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict["Thread0.Min"] = '%d' % int(pat_result[0][4])
	cur_dict["Thread0.Act"] = '%d' % int(pat_result[0][5])
	cur_dict["Thread0.Avg"] = '%d' % int(pat_result[0][6])
	cur_dict["Thread0.Max"] = '%d' % int(pat_result[0][7])
	cur_dict["Thread1.Min"] = '%d' % int(pat_result[1][4])
	cur_dict["Thread1.Act"] = '%d' % int(pat_result[1][5])
	cur_dict["Thread1.Avg"] = '%d' % int(pat_result[1][6])
	cur_dict["Thread1.Max"] = '%d' % int(pat_result[1][7])


sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'usec'))
