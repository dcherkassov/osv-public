#!/bin/python

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser') 
import common as plib

ref_section_pat = "^\[[\d\w_ .]+.[gle]{2}\]"
cur_search_str = "^([\d]{1,2})( objects =)(\ *)(\d{1,2}.?\d{1,2})(.*)$"

cur_dict = {}
cur_file = open(plib.CUR_LOG,'r')
print "Reading current values from " + plib.CUR_LOG
cur_raw_values = cur_file.readlines()
cur_file.close()

for cur_item in cur_raw_values:
	cur_match = re.match(cur_search_str, cur_item)
	if cur_match:
		cur_dict[cur_match.group(1)+"_objects"] = cur_match.group(4)

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'FPS'))
