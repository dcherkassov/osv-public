source ../scripts/overlays.sh
set_overlay_vars

source ../scripts/reports.sh
source ../scripts/functions.sh

source $TEST_HOME/../LTP/ltp.sh

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; ./rtc-test"  
}

function test_processing {
	log_compare "$TESTDIR" "3" "Passed" "p"
	log_compare "$TESTDIR" "0" "Fail" "n"
}

test_run
get_testlog $TESTDIR
test_processing
