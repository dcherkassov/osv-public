tarball=ipv6connect.tar.gz

function test_build {
    make CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put ipv6connect  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; ./ipv6connect"  
}

function test_processing {
	true
}

. ../scripts/functional.sh
