tarball=lmbench3.tar.gz

function test_build {
   mkdir -p SCCS
   touch SCCS/s.ChangeSet
   cd scripts
   patch -p0 < $TEST_HOME/lmbench3.config-run.patch
   patch -p0 < $TEST_HOME/lmbench.patch
   patch -p0 < $TEST_HOME/lmbench3.mem64.patch
   cd ../src
   patch -p0 < $TEST_HOME/bench.h.patch
   cd ..
   CFLAGS+=" -g -O"
   make OS="$PREFIX" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
   put -r *  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
   safe_cmd "rm -rf $OSV_HOME/osv.$TESTDIR/results"
   safe_cmd "cd $OSV_HOME/osv.$TESTDIR/scripts; OS=$PREFIX ./config-run"
   safe_cmd "cd $OSV_HOME/osv.$TESTDIR/scripts; OS=$PREFIX ./results"
   report "cd $OSV_HOME/osv.$TESTDIR/scripts; ./getsummary ../results/$PREFIX/*.0"  
}

. ../scripts/benchmark.sh
