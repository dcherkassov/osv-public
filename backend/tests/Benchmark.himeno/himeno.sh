tarball=himeno.tar.bz2

function test_build {
    CFLAGS+=" -O3"  
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
	put bmt  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR && ./bmt"  
}

. ../scripts/benchmark.sh
