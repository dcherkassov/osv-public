tarball=bzip2-1.0.5.tar.gz

function test_build {
    echo "#!/bin/bash
    if bzip2 -1  < sample1.ref > sample1.rb2; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;
    if bzip2 -2  < sample2.ref > sample2.rb2; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;
    if bzip2 -3  < sample3.ref > sample3.rb2; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;
    if bzip2 -d  < sample1.bz2 > sample1.tst; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;
    if bzip2 -d  < sample2.bz2 > sample2.tst; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;
    if bzip2 -ds < sample3.bz2 > sample3.tst; then echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi;
    if cmp sample1.bz2 sample1.rb2; then echo 'TEST-7 OK'; else echo 'TEST-7 FAILED'; fi;
    if cmp sample2.bz2 sample2.rb2; then echo 'TEST-8 OK'; else echo 'TEST-8 FAILED'; fi;
    if cmp sample3.bz2 sample3.rb2; then echo 'TEST-9 OK'; else echo 'TEST-9 FAILED'; fi;
    if cmp sample1.tst sample1.ref; then echo 'TEST-10 OK'; else echo 'TEST-10 FAILED'; fi;
    if cmp sample2.tst sample2.ref; then echo 'TEST-11 OK'; else echo 'TEST-11 FAILED'; fi;" > run-tests.sh
    touch test_suite_ready
}
    
function test_deploy {
    put {sample*,run-tests.sh}  $OSV_HOME/osv.$TESTDIR/
}
 
function test_run {   
    report "cd $OSV_HOME/osv.$TESTDIR; sh -v run-tests.sh 2>&1"  
}

function test_processing {    
    log_compare "$TESTDIR" "11" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

. ../scripts/functional.sh
