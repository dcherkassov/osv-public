source ../scripts/overlays.sh
set_overlay_vars

source ../scripts/reports.sh
source ../scripts/functions.sh

source $TEST_HOME/../LTP/ltp.sh

function test_run {
	report "cd /tmp/osv.$TESTDIR/target_bin; ./bin/run-all-posix-option-group-tests.sh"  
}

function test_processing {
	P_CRIT="execution: PASS"
	N_CRIT="execution: (FAIL|UNSUPPORTED|SIGNALED|UNTESTED|SKIPPED|UNRESOLVED|EXITED ABNORMALLY)"

	log_compare "$TESTDIR" "1327" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "169" "${N_CRIT}" "n"
}

test_run
get_testlog $TESTDIR
test_processing
