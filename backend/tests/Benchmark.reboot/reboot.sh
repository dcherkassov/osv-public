tarball=reboot

function test_build {
	true
}

function test_deploy {
	put $TEST_HOME/$tarball  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	target_reboot
	report "cd $OSV_HOME/osv.$TESTDIR; ./reboot"  
}

. ../scripts/benchmark.sh