#!/bin/python

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib


ref_section_pat = "^\[[\w\d_ ./]+.[gle]{2}\]"
	
# Jan  6 09:55:31 2013 (none) user.notice bootlog: [     2.227600] bootlogd.

cur_search_pat = re.compile(" user.info kernel: \[ *([\d.]+)\] Freeing init memory: 264K\n")

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	for item in pat_result:
		cur_dict["time.init"] = item[0]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', ' '))
