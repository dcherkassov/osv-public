tarball=interbench-0.31.tar.bz2

function test_build {
    patch -p0 < $TEST_HOME/interbench.c.patch
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || exit 1
}

function test_deploy {
	put interbench  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; ./interbench -L 1 || ./interbench -L 1"  
}

. ../scripts/benchmark.sh
