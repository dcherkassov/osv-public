tarball=java_perf.tar

function test_build {
    touch test_suite_ready
}

function test_deploy {
    put *.jar  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR; java -cp scimark2lib.jar jnt.scimark2.commandline"  
    report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar avrora" $OSV_HOME/ 
    report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar jython"  
    report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar luindex"  
    report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar lusearch"  
    #report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar tomcat"  
    report_append "cd $OSV_HOME/osv.$TESTDIR; java -jar dacapo-9.12-bach.jar xalan"  
}

. ../scripts/benchmark.sh
