#!/bin/python

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib


ref_section_pat = "^\[[\w\d._]+.[gle]{2}\]"
cur_search_pat = re.compile("(.*)([()\w=,\r\n\s\d]+)(\s*:\s*)(\d{1,4}.\d+)$|(===== DaCapo 9.12 )(\w+)( PASSED in )(\d{1,7})",re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	iter = 0
	for item in pat_result:
		if 'SciMark' in item[0]:
			iter = 1
		if (0 < iter <= 6):
			if iter == 1:
				cur_dict['SciMark.Composite_Score'] = "%.2f" % float(item[-5])
				iter += 1
			elif iter == 2:
				cur_dict['SciMark.FFT'] = "%.2f" % float(item[-5])
				iter += 1
			elif iter == 3:
				cur_dict['SciMark.SOR'] = "%.2f" % float(item[-5])
				iter += 1
			elif iter == 4:
				cur_dict['SciMark.Monte_Carlo'] = "%.2f" % float(item[-5])
				iter += 1
			elif iter == 5:
				cur_dict['SciMark.Sparse_matmult'] = "%.2f" % float(item[-5])
				iter += 1
			elif iter == 6:
				cur_dict['SciMark.LU'] = "%.2f" % float(item[-5])
				iter += 1

		if 'DaCapo' in item[4]:
			cur_dict['DaCapo.'+item[5]] = item[-1]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'FPS, %'))
