#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys
sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d_\- .]+.[gle]{2}\]"
cur_search_pat = re.compile("^([\s\d]+)reps \@\s*([\d]+\.[\d]+)\s*msec\s*\(\s*([\d]+\.[\d]+)/sec\s*\):\s*(.*)$",re.MULTILINE)

res_dict = {}
cur_dict = {}

pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict["dot"] = '%.2f' % float(pat_result[0][2])
	cur_dict["oddtilerect10"] = '%.2f' % float(pat_result[1][2])
	cur_dict["seg100c2"] = '%.2f' % float(pat_result[2][2])
	cur_dict["64poly10complex"] = '%.2f' % float(pat_result[3][2])


print cur_dict

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'SPS'))
