tarball=osdl-aim-7.0.1.13.tar.gz

function test_build {
        ./bootstrap
        PKG_CONFIG_PATH=${SDKROOT}/usr/lib/pkgconfig PKG_CONFIG_ALLOW_SYSTEM_LIBS=1 PKG_CONFIG_SYSROOT_DIR=${SDKROOT} ./configure --host=$HOST --build=`uname -m`-linux-gnu LDFLAGS=-L${SDKROOT}/usr/lib CPPFLAGS=-I${SDKROOT}/usr/include  CFLAGS=-I${SDKROOT}/usr/include LIBS=-laio --prefix=$OSV_HOME/$TESTDIR --datarootdir=$OSV_HOME/$TESTDIR
        make && touch test_suite_ready || exit 1
}

function test_deploy {
	put src/reaim  $OSV_HOME/osv.$TESTDIR/
	put -r data  $OSV_HOME/osv.$TESTDIR/
	put -r scripts  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; mkdir /tmp/diskdir; ./reaim -c ./data/reaim.config -f ./data/workfile.short"  
	report_append "cd $OSV_HOME/osv.$TESTDIR; ./reaim -c ./data/reaim.config -f ./data/workfile.all_utime"  
}

. ../scripts/benchmark.sh
