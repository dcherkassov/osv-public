source ../scripts/overlays.sh
set_overlay_vars

source ../scripts/reports.sh
source ../scripts/functions.sh

source $TEST_HOME/../LTP-DDT/ltp-ddt.sh

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR/testscripts; ./nptl.sh"  
}

function test_processing {
    P_CRIT="PASS"
    log_compare "$TESTDIR" "4" "${P_CRIT}" "p"
}

test_run
get_testlog $TESTDIR
test_processing
