#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys
sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib

ref_section_pat = "^\[[\w\d_ .]+.[gle]{2}\]"

cur_search_pat_main = re.compile("^\s*(readall|create|append|delete|stat|writeall|writeall_fsync|open_close|create_fsync|append_fsync)\s{1}:\s*(\d+\.?\d*)\s*(\d+\.?\d*)\s*(\d+\.?\d*)%\s*(\d+\.?\d*)%\s*(NA|\d+\.?\d*).*$",re.MULTILINE)
cur_search_pat_throughput = re.compile("^(Read|Write)\s{1}Throughput:\s*(\d+\.?\d*)(\D{1})B/sec$",re.MULTILINE)
cur_search_pat_syscalls_latency = re.compile("^\[\s*(\D+)\]\s*(\d+\.?\d*)\s*(\d+\.?\d*)\s*(\d+\.?\d*).*$",re.MULTILINE)

res_dict = {}
cur_dict = {}

pat_result_main = plib.parse(cur_search_pat_main)
if pat_result_main:
	for test in pat_result_main:
		print test
		cur_dict["Main."+test[0]+".Transactions"] = test[1]
		cur_dict["Main."+test[0]+".TPS"] = test[2]
		cur_dict["Main."+test[0]+".TPercent"] = test[3]
		cur_dict["Main."+test[0]+".OpWeight"] = test[4]		


pat_result_throughput = plib.parse(cur_search_pat_throughput)
if pat_result_throughput:
	print pat_result_throughput
	if pat_result_throughput[0][2] == 'K':
		r_mul = 1;
	elif pat_result_throughput[0][2] == 'M':
		r_mul = 1000;
	if pat_result_throughput[1][2] == 'K':
		w_mul = 1;
	elif pat_result_throughput[1][2] == 'M':
		w_mul = 1000;
	cur_dict["Throughput.Read"] = '%.2f' % (float(pat_result_throughput[0][1]) * r_mul)
	cur_dict["Throughput.Write"] = '%.2f' % (float(pat_result_throughput[1][1]) * w_mul)


pat_result_syscalls_latency = plib.parse(cur_search_pat_syscalls_latency)
if pat_result_syscalls_latency:
	for test in pat_result_syscalls_latency:
		print test
		cur_dict["Syscall_latency."+test[0]+".Min"] = test[1]
		cur_dict["Syscall_latency."+test[0]+".Avg"] = test[2]
		cur_dict["Syscall_latency."+test[0]+".Max"] = test[3]

print cur_dict

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', ''))
