#!/bin/python
# See common.py for description of command-line arguments

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser')
import common as plib

ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_pat = re.compile("^(Your GLMark08 Score is )([\d]{1,3})",re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict["GLMark_Score"] = pat_result[0][1]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'FPS'))
