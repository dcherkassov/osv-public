tarball=iozone3_353.tar

function test_build {
    cd src/current

    if [ "$ARCHITECTURE" == "ia32" ]; then
        TARGET=linux
    elif [ "$ARCHITECTURE" == "arm" ]; then
        TARGET=linux-arm
    else
        echo "platform based on $ARCHITECTURE is not supported by benchmark"
        exit 1
    fi

    make $TARGET GCC="$CC" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || exit 1
}


function test_deploy {
    cd src/current
    put fileop iozone pit_server  $OSV_HOME/osv.$TESTDIR/
}

# function test_run {
#     report "cd $OSV_HOME/osv.$TESTDIR; ./iozone -a -i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9 -O -R -g $file_size" $OSV_HOME/osv.$TESTDIR/$TESTDIR_tmp.log
#     safe_cmd "cd $OSV_HOME/osv.$TESTDIR; cat $OSV_HOME/osv.$TESTDIR/$TESTDIR_tmp.log | tail -n 139 | tee  "
# }

function test_run {
    assert_define BENCHMARK_IOZONE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_IOZONE_MOUNT_POINT
    assert_define BENCHMARK_IOZONE_FILE_SIZE
    
    hd_test_mount_prepare $BENCHMARK_IOZONE_MOUNT_BLOCKDEV $BENCHMARK_IOZONE_MOUNT_POINT

    report "cd $BENCHMARK_IOZONE_MOUNT_POINT; $OSV_HOME/osv.$TESTDIR/iozone -a -i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9 -O -R -g $BENCHMARK_IOZONE_FILE_SIZE" $OSV_HOME/osv.$TESTDIR/Benchmark.IOzone.log
    safe_cmd "cd $OSV_HOME/osv.$TESTDIR; cat $OSV_HOME/osv.$TESTDIR/Benchmark.IOzone.log | tail -n 139 | tee  "

    hd_test_clean_umount $BENCHMARK_IOZONE_MOUNT_BLOCKDEV $BENCHMARK_IOZONE_MOUNT_POINT
}


. ../scripts/benchmark.sh
