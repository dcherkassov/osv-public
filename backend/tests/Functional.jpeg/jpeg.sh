tarball=jpeg-6b.tar.gz

function test_build {
    ./configure --host=$PREFIX --build=`./config.guess` CC="$CC" AR="$AR" RANLIB="$RANLIB"
    make
    echo "#!/bin/bash
    if ./djpeg -dct int -ppm -outfile testout.ppm testorig.jpg; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
    if ./djpeg -dct int -bmp -colors 256 -outfile testout.bmp testorig.jpg; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi ;
    if ./cjpeg -dct int -outfile testout.jpg  testimg.ppm; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi;
    if ./djpeg -dct int -ppm -outfile testoutp.ppm testprog.jpg; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi;
    if ./cjpeg -dct int -progressive -opt -outfile testoutp.jpg testimg.ppm; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi;
    if ./jpegtran -outfile testoutt.jpg testprog.jpg; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi;
    if cmp testimg.ppm testout.ppm; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi;
    if cmp testimg.bmp testout.bmp; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi;
    if cmp testimg.jpg testout.jpg; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi;
    if cmp testimg.ppm testoutp.ppm; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi;
    if cmp testimgp.jpg testoutp.jpg; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi;
    if cmp testorig.jpg testoutt.jpg; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi;" > run-tests.sh
    touch test_suite_ready
}

function test_deploy {
    put run-tests.sh test* djpeg cjpeg jpegtran  $OSV_HOME/osv.$TESTDIR/;
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR; sh -v run-tests.sh"  
}

function test_processing {
    log_compare "$TESTDIR" "12" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}

. ../scripts/functional.sh
