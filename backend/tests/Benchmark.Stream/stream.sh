tarball=stream.tar.bz2

function test_build {
	make stream_c.exe CFLAGS+="${CFLAGS}" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || exit 1
}

function test_deploy {
	put stream_c.exe  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; ./stream_c.exe"  
}

. ../scripts/benchmark.sh
