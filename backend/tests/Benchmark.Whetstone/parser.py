#!/bin/python

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser') 
import common as plib

ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_pat = re.compile("^(C Converted Double Precision Whetstones:)(\ )([\d]{1,4}.?[\d]{1,2})(.*)$", re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	cur_dict['Whetstone'] = pat_result[0][2]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'MIPS'))
