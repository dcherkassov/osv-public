tarball=Whetstone.tar.bz2

function test_build {
  	CFLAGS+=" -DTIME"
 	make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" LIBS=" -lm" && touch test_suite_ready || exit 1
}

function test_deploy {
	put whetstone  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_WHETSTONE_LOOPS
	report "cd $OSV_HOME/osv.$TESTDIR && ./whetstone $BENCHMARK_WHETSTONE_LOOPS"  
}

. ../scripts/benchmark.sh
