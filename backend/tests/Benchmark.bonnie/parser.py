#!/bin/python

import os, re, sys

sys.path.insert(0, '/home/jenkins/scripts/parser') 
import common as plib

ref_section_pat = "^\[[\w_ .]+.[gle]{2}\]"

cur_dict = {}
cur_file = open(plib.CUR_LOG,'r')
print "Reading current values from " + plib.CUR_LOG + "\n"

raw_values = cur_file.readlines()
results = raw_values[-1].rstrip("\n").split(",")

cur_file.close()

if len(results) < 26:
	sys.exit("\nOSV error reason: No results found\n")

cur_dict["Sequential_Output.PerChr"] = results[2]
cur_dict["Sequential_Output.Block"] = results[4]
cur_dict["Sequential_Output.Rewrite"] = results[6]
cur_dict["Sequential_Input.PerChr"] = results[8]
cur_dict["Sequential_Create.Create"] = results[15]
cur_dict["Sequential_Create.Delete"] = results[19]
cur_dict["Random_Create.Create"] = results[21]
cur_dict["Random_Create.Delete"] = results[25]

for x in cur_dict:
	print x
	if '+' in cur_dict[x]:
		cur_dict[x] = '0'

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Rate, MB/s'))
