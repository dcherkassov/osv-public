#!/bin/python

import os, re, sys, json

sys.path.insert(0, '/home/jenkins/scripts/parser') 
import common as plib

cur_dict = {}
cur_file = open(plib.CUR_LOG,'r')
print "Reading current values from " + plib.CUR_LOG + "\n"

ref_section_pat = "^\[[\w_ .]+.[gle]{2}\]"

raw_values = cur_file.readlines()
results = raw_values[-1].rstrip("\n").split(",")
cur_file.close()

cur_dict["result1"] = results[0]
cur_dict["result2"] = results[1]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'value'))

