tarball=hackbench.tar.gz

function test_build {
    $CC -lpthread hackbench.c -o hackbench && touch test_suite_ready || exit 1
}

function test_deploy {
	put hackbench  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	report "cd $OSV_HOME/osv.$TESTDIR; ./hackbench $groups"  
}

. ../scripts/benchmark.sh