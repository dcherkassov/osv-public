tarball=cyclictest.tar.gz

function test_build {
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
	put cyclictest  $OSV_HOME/osv.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_CYCLICTEST_LOOPS
	report "cd $OSV_HOME/osv.$TESTDIR; ./cyclictest -t 2 -l $BENCHMARK_CYCLICTEST_LOOPS -q"  
}

. ../scripts/benchmark.sh
