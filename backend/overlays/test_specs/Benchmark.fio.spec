{
    "testName": "Benchmark.fio",
    "specs": 
    [
        {
            "name":"sata",
            "MOUNT_BLOCKDEV":"$SATA_DEV",
            "MOUNT_POINT":"$SATA_MP",
            "TIMEOUT":"10"
        },
        {
            "name":"mmc",
            "MOUNT_BLOCKDEV":"$MMC_DEV",
            "MOUNT_POINT":"$MMC_MP",
            "TIMEOUT":"10"
        },
        {
            "name":"usb",
            "MOUNT_BLOCKDEV":"$USB_DEV",
            "MOUNT_POINT":"$USB_MP",
            "TIMEOUT":"10"
        },
        {
            "name":"default",
            "MOUNT_BLOCKDEV":"ROOT",
            "MOUNT_POINT":"$OSV_HOME/work",
            "TIMEOUT":"10"
        }

    ]
}

                

                      
