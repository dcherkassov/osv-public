{
    "testName": "Benchmark.ffsb",
    "specs": 
    [
        {
            "name":"sata",
            "MOUNT_BLOCKDEV":"$SATA_DEV",
            "MOUNT_POINT":"$SATA_MP"
        },
        {
            "name":"mmc",
            "MOUNT_BLOCKDEV":"$MMC_DEV",
            "MOUNT_POINT":"$MMC_MP"
        },
        {
            "name":"usb",
            "MOUNT_BLOCKDEV":"$USB_DEV",
            "MOUNT_POINT":"$USB_MP"
        },
        {
            "name":"default",
            "MOUNT_BLOCKDEV":"ROOT",
            "MOUNT_POINT":"$OSV_HOME/work"
        }
    ]
}

                

                      
