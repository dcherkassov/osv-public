#!/usr/bin/python

# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import sys, os, re
import simplejson as json

JOB_NAME=sys.argv[1]
DATAFILE=sys.argv[2]
REF_DATA=sys.argv[3]
RESULT_PATH='/home/jenkins/logs/'+JOB_NAME+'/'

CUR_DEVICE=os.getenv('NODE_NAME')

# Read reference log file
ref_section_pat = "^\[[\w\d&._/()-]+.[gle]{2}\]"
ref_file = open(REF_DATA,'r')
ref_raw_data = ref_file.readlines()
ref_file.close

groups = {}
info = {}
devices = []

result = []
# Array of "device"
# result = [{'device':'','fw':[],'sdk':[],'tests':[{'data':[],'label':''},{'data':[],'label':''}]}]

# Read raw data array of stings, reference log
fp = open(DATAFILE,'r')
data = fp.readlines()
fp.close

for item in ref_raw_data:
	try:
		item = item[:item.index('#')].strip()  # strip comments
	except ValueError: # no comment found
		pass
	if (re.match(ref_section_pat, item)):
		# Get section info: test_group_name.test_case_name|criteria
		full_section = item.lstrip('[').rstrip(']\n')
		# Get test case name
		section, criteria = full_section.split('|') # as <section_name>|<criteria>
		# Get group name and test case name
		try:
			groupname, test = section.split('.') # as section_name = group_name.test_name
		except ValueError:
			groupname = section
			test = section

		for line in data:
			spd = line.split()
			device_name = spd[7] if len(spd) == 8 else "Unknown"
			
			if (not groups.get(groupname)):
				groups[groupname] = [{'data':[],'points':{ 'symbol': "circle" },'label':device_name+'-'+groupname+'.'+test},
									{'data':[],'points':{ 'symbol': "cross" },'label':device_name+'-'+groupname+'.'+test+'.ref'}]
			
			flag = False
			for i in (groups.get(groupname)):
				if (device_name+'-'+groupname+'.'+test == i.get('label')):
					flag = True
					break
			
			if flag == False:
				groups.get(groupname).append({'data':[],'points':{ 'symbol': "circle" }, 'label':device_name+'-'+groupname+'.'+test})
				groups.get(groupname).append({'data':[],'points':{ 'symbol': "cross" }, 'label':device_name+'-'+groupname+'.'+test+'.ref'})
			

# Now I have the floowing:
# GROUP:Max_Switch_Time 
# GROUP CONTENT: [
# {'points': {'symbol': 'circle'}, 'data': [], 'label': 'TESTNAME'}, 
# {'points': {'symbol': 'cross'}, 'data': [], 'label': 'TESTNAME.ref'}
# repeated GROUPS_NUMBER times
# ]

# Read log file with approx. strucure:
# <date><bid><group.test><cur><ref>[<fw><sdk><dev>]

for line in data:
	spd = line.split()
#	print spd
	device_name = spd[7] if len(spd) == 8 else "Unknown"
	try:
		grp, testname = spd[2].split('.') # Always in log
	except ValueError:
		grp = spd[2]
		testname = spd[2]

	for item in groups.get(grp):
		if '.ref' in item['label']:
			type = item['label'].replace('.ref','')
			if (device_name+'-'+grp+'.'+testname == type):
				item['data'].append((spd[1],float(spd[3])))
		elif (device_name+'-'+grp+'.'+testname in item['label']):
				item['data'].append((spd[1],float(spd[4])))
		
		if info.has_key(spd[1]) == False:
			if len(spd) == 8:
				info[spd[1]] = [spd[1], spd[5], spd[6], device_name] # bid, FW, SDK, device
			else:
				info[spd[1]] = [spd[1], "Unknown", "Unknown", "Unknown"]

for m in sorted(info.keys()):
	string = info.get(m)

	bid = string[0]
	fw = string[1]
	sdk = string[2]
	dev = string[3]

	if len(devices) == 0:
		devices.append({'device':dev,'info':[[bid],[fw],[sdk]]})

	flag = False
	for i in devices:
		if dev == i.get('device'):
			flag = True
			d = i

	if flag:
		d.get('info')[0].append(bid)
		d.get('info')[1].append(fw)
		d.get('info')[2].append(sdk)

	if flag == False:
		devices.append({'device':dev,'info':[[bid],[fw],[sdk]]})

devices.sort()

for a in sorted(set(groups)):
	RESULT = RESULT_PATH+JOB_NAME+'.'+a+'.json'
	print "Writing results to ", RESULT
	rf = open(RESULT,'w')
	rf.write(json.dumps(groups.get(a),sort_keys=True))
	rf.close

INF_FILE = RESULT_PATH+JOB_NAME+'.info.json'
inf = open(INF_FILE,'w')
print "Writing info file:", RESULT_PATH+JOB_NAME+'.info.json'
inf.write(json.dumps(devices,sort_keys=True))
inf.close

def main():
	pass

if __name__ == '__main__':
	main()
