# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os, sys

# Search for plot.data files
search_root = "/home/jenkins/logs"
for root, dirs, files in os.walk(search_root):
	for file in files:
		if 'plot.data' == file:
			print os.path.join(root,file)
# Read file and search for build number
			datafp = open(os.path.join(root,file),'r')
			bn = 0 # init
			cnt = 0 # duplicate counter
			prev_cnt = 0
			for line in datafp.readlines():
				if bn == int(line.split()[1]):
					cnt += 1
				else:
					if (prev_cnt != cnt) and (cnt != 0) and (prev_cnt != 0):
						print "Build number counter error for build "+str(bn)+".\tPrevious number = "+str(prev_cnt)+".\tCurrent number = "+str(cnt)
					prev_cnt = cnt
					cnt = 1
					if (bn+1 != int(line.split()[1])) and (bn != 0):
						print "Wrong build order. "
					bn = int(line.split()[1])
			datafp.close()
