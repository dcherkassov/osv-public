#!/usr/bin/env python

# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# Parameters for this script are: JOB_NAME, PLATFORM, TARBALL_TEMPLATE,
# BUILD_ID, BUILD_NUMBER, Number of last builds to plot
#
# encoding: utf-8
# vim:set tabstop=2:set autoindent:
# set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<:list
"""
common.py - This is parsing functions library.
Created by Konstantin Belov on 2011-07-28.
Generalized by Dmitry Semyonov
"""

import sys, os, re, json

import matplotlib
matplotlib.use('Agg')
import pylab as plot

JENKINS_HOME="/home/jenkins"

JOB_NAME=sys.argv[1]
PLATFORM=sys.argv[2] # ignored for backward compatibility
BUILD_ID = sys.argv[3]
BUILD_NUMBER = sys.argv[4]
#BUILDS_NUM = sys.argv[6]
FIRMWARE = sys.argv[5]
SDK = sys.argv[6]
DEVICE = sys.argv[7]

# XXX: This way of getting required parameters should be implemented
# XXX: as more preferable, but there should be a way to run parser
# XXX: from command line w/o environment variables setup.

#BUILD_ID = os.environ['BUILD_ID']
#BUILD_NUMBER = os.environ['BUILD_NUMBER']
#PLATFORM = os.environ['Platform_SDK'].split('/')[1].lstrip(' ')
#JOB_NAME = os.environ['JOB_NAME']
#FIRMWARE = os.environ['FW']
#DEVICE = os.environ['DEVICE']

PLOT_DATA=JENKINS_HOME+'/logs/'+JOB_NAME+'/plot.data'
PLOT_FILE = JENKINS_HOME+'/logs/'+JOB_NAME+'/plot.png'

REF_LOG=JENKINS_HOME+'/tests/'+JOB_NAME+'/'+'reference.log'
CUR_LOG=JENKINS_HOME+'/logs/'+JOB_NAME+'/testlogs/'+DEVICE+'.'+BUILD_ID+'.'+BUILD_NUMBER+'.log'  

rdict = {} # reference values
cdict = {} # decision criteria

custom_write_report = False # the test will use custom methods to write report

def parse(cur_search_pat):
        print "Reading current values from " + CUR_LOG +"\n"
        cur_file = open(CUR_LOG, 'r')
        pat_result = cur_search_pat.findall(cur_file.read())
        cur_file.close()
        return pat_result


def write_report_results(rep_data):
        if 'GEN_TESTRES_FILE' in os.environ and (os.environ['GEN_TESTRES_FILE'] != ""):
                print "Using default report writing function\n"
                with open(os.environ['GEN_TESTRES_FILE'], 'w+') as f:
                        f.write(json.dumps(rep_data, indent=4))
                        print ("Wrote %s testres file" % (os.environ['GEN_TESTRES_FILE']))
        else:
                print ("Not writing testres file")


def process_data(ref_section_pat, cur_dict, m, label):
	if not cur_dict:
		print "\nOSV error reason: could not parse test results in %s\n" % CUR_LOG
		sys.exit(1)

        if custom_write_report == False:
                write_report_results(cur_dict)

        read_ref_data(ref_section_pat)

        if (set(rdict) - set(cdict)):
                hls("ERROR: Results are missing in logfile.","e")
                sys.exit(1)

        rc = compare(rdict, cur_dict, cdict)
        store_plot_data(rdict, cur_dict)
        plot_set_props(m)
        if m == 's':
            create_plot(label)
        else:
            create_multiplot(label)

        print "Saving plot file to " + PLOT_FILE
        plot.savefig(PLOT_FILE)

        return rc

def plot_set_props(m):
	"""Defines plot parameters such as font size, figure size, etc to default values.""" 	
	plot.rcParams['font.family'] = 'sans-serif'
	plot.rcParams['font.sans-serif'] = 'Helvetica'
	plot.rcParams['axes.titlesize'] = 14.0
	plot.rcParams['legend.fontsize'] = 10.0
	plot.rcParams['figure.dpi'] = 100
	plot.rcParams['figure.facecolor'] = 'w'
	plot.rcParams['figure.edgecolor'] = 'k'
	if (m == "s"):
		plot.rcParams['font.size'] = 14.0
		plot.rcParams['figure.figsize'] = 19,6
	elif (m == "m"):
		plot.rcParams['font.size'] = 14.0
		plot.rcParams['figure.figsize'] = 19,10
	elif (m == "l"):
		plot.rcParams['font.size'] = 14.0
		plot.rcParams['figure.figsize'] = 19,12
	elif (m == "xl"):
		plot.rcParams['font.size'] = 14.0
		plot.rcParams['figure.figsize'] = 19,35


def read_ref_data(section_pat):
	"""Reads test names (defined by section_pat) and values from REF_LOG 
	   into reference result dictionary."""  

	if os.path.isfile(REF_LOG):
		print "------------------------------------------------------------\n"
		print "Reading reference data from " + REF_LOG + "\n" 

		ref_file = open(REF_LOG,'r')
		ref_raw_data = ref_file.readlines()

		for item in ref_raw_data:
			try:
				item = item[:item.index('#')].strip()  # strip comments
			except ValueError: # no comment found
				pass
			if (re.match(section_pat, item)):
				full_section = item.lstrip('[').rstrip(']\n')
				section, criteria = full_section.split('|') # as <section_name>|<criteria>
				rdict[section] = ''
				cdict[section] = criteria

			# threshold value
			if (re.match("[0-9.]+", item)):
				rdict[section] = item.rstrip('\n')
		ref_file.close()
	else:
		hls("Log file "+REF_LOG+" not found.","w")
		sys.exit(1)

	if (len(rdict) == 0):
		hls("Reference file "+REF_LOG+" is empty.","e")
		sys.exit(1)

def store_plot_data(r_dict, c_dict):
	'''
	Stores data from dictionaries into file.
	'''
	if os.path.isfile(PLOT_DATA):
		plot_file = open(PLOT_DATA,"r+") # Append
		data = plot_file.readlines()
		if len(data) > 0: # File exists and not empty
			if (data[-1].split()[1] != BUILD_NUMBER): # Check presents of build data in file
				fill_data (plot_file,r_dict,c_dict)
			else:
				print "\nOSV error reason: %s already contains data for build #%s\n" % (PLOT_DATA, BUILD_NUMBER)
	else:
		plot_file = open(PLOT_DATA,"w") # Create new
		fill_data (plot_file,r_dict,c_dict)

	plot_file.close()

def fill_data (plot_file,r_dict,c_dict):
	'''
	Fills plot.data file with given values and the following format:
	BUILD_ID BUILD_NUMBER TESTNAME REF.VALUE CUR.VALUE FW SDK DEVICE
	'''
	for key in sorted(r_dict.iterkeys()):
		r_split = r_dict.get(key).split()
		c_split = c_dict.get(key).split()
		en_ref_dict = enumerate(r_split)
		for i,u in en_ref_dict:
			plot_file.write(BUILD_ID+' '+BUILD_NUMBER+' '+key+' '+r_split[i]+' '+c_split[i]+' '+FIRMWARE+' '+SDK+' '+DEVICE+'\n')
	print "\nData file "+PLOT_DATA+" was updated."

def create_plot(ylabel):
	"""Generates plot from datafile. ylabel - label for Y-axis """
	if os.path.isfile(PLOT_DATA):
		print "\nReading plot data from " + PLOT_DATA
		plot_file = open (PLOT_DATA,'r')
		plot_data = plot_file.readlines()
		ref_data = {}
		test_data = {}
		b_data = []
		t_data = []
		for item in plot_data:
			data_split = item.split()
			if (data_split[1] not in b_data):
				b_data.append(data_split[1])
			if (data_split[2] not in t_data):
				t_data.append(data_split[2])
			# Fill dict with reference data	
			if ref_data.get(data_split[2]):
				ref_data[data_split[2]] = ref_data.get(data_split[2])+','+data_split[3]
			else:
				ref_data[data_split[2]] = data_split[3]
			# Fill dict with test data
			if test_data.get(data_split[2]):
				test_data[data_split[2]] = test_data.get(data_split[2])+','+data_split[4]
			else:
				test_data[data_split[2]] = data_split[4]
		
		# Figure configuration
		ax = plot.figure().add_subplot(111)
		ax.set_xlabel('Build number')
		ax.set_ylabel(ylabel) 
		ax.set_title(JOB_NAME)
		box = ax.get_position()
		ax.set_position([box.bounds[0], box.bounds[1], box.bounds[2] * 0.8, box.bounds[3]])
		# Put a legend to the right of the current axis
		ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
		ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
		# Hide these grid behind plot objects
		ax.set_axisbelow(True)
		bnum = int(BUILD_NUMBER)
		plotrange = b_data[-bnum:]

		for key in sorted(t_data):
		# It is required to start not from index #0 but from #1
			ax.plot(plotrange, test_data.get(key).split(",")[-bnum:], 'o-', label='Test '+key)

		# Store min/max values for test data
		ymin, ymax = plot.ylim()

		for key in sorted(t_data):
			ax.plot(plotrange, ref_data.get(key).split(',')[-bnum:], 'x--', label='Ref. '+key)

		# Set Y size based on test data, adding 7% to display properly set reference values
		plot.ylim(ymin * 0.93, ymax * 1.07)
		plot.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	else:
		hls("Plot data file not found","e")
		sys.exit(1)

def create_multiplot(ylabel):
	"""
	Generates plot from datafile.
	ylabel - label for Y-axis
	"""
	if os.path.isfile(PLOT_DATA):
		print "\nReading plot data from " + PLOT_DATA
		plot_file = open (PLOT_DATA,'r')
		plot_data = plot_file.readlines()
		if (len(plot_data) == 0):
			hls("Plot data file "+PLOT_DATA+" is empty.","e")
			sys.exit(1)
		else:
			ref_data = {}
			test_data = {}
			tests = {}
			b_data = [] # The list of detected build numbers
			t_data = [] # The list of detected test names
			for item in plot_data:
				data_split = item.split()
				if len(data_split) >= 5:
					# Skipping line if there is missing field
					if (data_split[1] not in b_data):
						b_data.append(data_split[1])
					if (data_split[2] not in t_data):
						t_data.append(data_split[2])
					# Fill dict with reference data	
					if ref_data.get(data_split[2]):
						ref_data[data_split[2]] = ref_data.get(data_split[2])+','+data_split[3]
					else:
						ref_data[data_split[2]] = data_split[3]
					#Fill dict with test 
					if test_data.get(data_split[2]):
						test_data[data_split[2]] = test_data.get(data_split[2])+','+data_split[4]
					else:
						test_data[data_split[2]] = data_split[4]
				else:
					print "Skipping line as some fields are missing."
			for rkey in sorted(ref_data.iterkeys()):
#				print "DEBUG: <rkey>"+rkey
				rkey_split = rkey.split(".")
				
				if (len(rkey_split) > 1):
				# Fill dict with reference data 
					if tests.get(rkey_split[0]):
						tests[rkey_split[0]] = tests.get(rkey_split[0])+','+".".join(rkey_split[1:])
					else:
						tests[rkey_split[0]] = ".".join(rkey_split[1:])
			
			pn = 0
			t_num = len (tests)
			if t_num > 4:
				h_space = 0.6
			else:
				h_space = 0.4

			#print "DEBUG:" + str(tests)
			for testgroup in tests:
				pn += 1
				tests_split = tests.get(testgroup).split(",")
				plot.figure(1).subplots_adjust(hspace = h_space, wspace = 0.5, left = 0.055, right  = 0.65)
				plot.figure(1).text(0.5, 0.95, JOB_NAME,horizontalalignment='center',verticalalignment='bottom', fontsize=20)
				if (pn > 1):
					ax = plot.figure(1).add_subplot(t_num,1,0+pn, sharex=ax)
					# TODO: Need to create a list of test measument units.
					#ax.set_ylabel(str(pn))
				else:
					ax = plot.figure(1).add_subplot(t_num,1,0+pn, title=str(pn))
				ax.set_title(testgroup)
				if (pn == t_num):
					ax.set_xlabel('Build number')
				
				ax.set_ylabel(ylabel)
				box = ax.get_position()
				ax.set_position([box.bounds[0], box.bounds[1], box.bounds[2], box.bounds[3]])
				# Put a legend to the right of the current axis
				ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
				ax.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
				# Hide these grid behind plot objects
				ax.set_axisbelow(True)
				bnum = int(BUILD_NUMBER)
				plotrange = b_data[-bnum:]

#				print "DEBUG: <plotrange> "+str(plotrange)
#				print "DEBUG:<ref_data> %d"
#				for key in ref_data:
#					print "%s : %s" % (key, ref_data[key])
#				
#				print "DEBUG:<test_data>"
#				for key in test_data:
#					print "%s : %s" % (key, test_data[key])
				for testname in tests_split:
					# print "DEBUG: <testname>"+str(testname)
					test_num = len(tests_split)
					skey = testgroup+'.'+testname
					# print "DEBUG <skey>: "+skey
					# print "DEBUG <plotrange>: "+str(plotrange)
					# print "DEBUG <test_data>: "+str(test_data)
					# print "DEBUG <test_data>: "+str(test_data.get(skey).split(","))
					# print "Debug split:"+str(test_data.get(skey).split(",")[-bnum:])
					if len(plotrange) != len(ref_data.get(skey).split(",")[-bnum:]):
						sys.exit("There is problem with data integrity in "+PLOT_DATA+" file.")
					else:
						ax.plot(plotrange, test_data.get(skey).split(",")[-bnum:], 'o-', label='Test '+testname)

				# Store min/max values for test data
				ymin, ymax = plot.ylim()

				for testname in tests_split:
#					print "DEBUG: <testname>"+str(testname)
					test_num = len(tests_split)
					skey = testgroup+'.'+testname
					if len(plotrange) != len(ref_data.get(skey).split(",")[-bnum:]):
						sys.exit("There is problems with data integrity in "+PLOT_DATA+" file.")
					else:
						ax.plot(plotrange, ref_data.get(skey).split(",")[-bnum:], 'x--', label='Ref. '+testname)

				# Set Y size based on test data, adding 7% to display properly set reference values
				plot.ylim(ymin * 0.93, ymax * 1.07)

				if test_num > 4:
					lnc = 2
				else:
					lnc = 1
				plot.legend(loc = 2,ncol = lnc, bbox_to_anchor=(1.02, 1.), borderaxespad=0.)
	else:
		print "Data file" + PLOT_DATA + " is missing."

def hls(string,type):

	if type == "w":
		type_string = "WARNING"
	else:
		type_string = "ERROR"

	print "########################### " + type_string + " ###############################"
	print string
	print "-----"

def compare(ref_dict,cur_dict,crit_dict):
	"""
	This function makes a decision about current results.
	Input:reference results dictionary, current results dictionary, criteria dictionary.
	"""
	rc = 0
	for key in ref_dict:
		print key
		ref_split = ref_dict.get(key).split()
		cur_split = cur_dict.get(key).split()
		en_ref_dict = enumerate(ref_split)

		print cur_split		

		for i,u in en_ref_dict:
			print cur_split[i], ref_split[i]
			comparison_result = cmp(float(cur_split[i]),float(ref_split[i]))

			if crit_dict[key] == 'ge':
				crit_string = "greater or equal"
			elif crit_dict[key] == 'le':
				crit_string = "less or equal"

			if (crit_dict[key] == 'ge' and comparison_result < 0) or (crit_dict[key] == 'le' and comparison_result > 0):
				hls("For test "+key+" current value is "+cur_split[i]+", but reference value - "+ref_split[i]+".\nComparison criteria is \""+crit_string+"\".\n",'e')
				rc = 1
			else:
				print "For test "+key+" current value is "+cur_split[i]+", reference value - "+ref_split[i]+". Result - OK.\nComparison criteria is \""+crit_string+"\".\n"

	if rc == 1:
		print "OSV error reason: Unmet threshold(s)"

	return rc
			
def main():
	pass

if __name__ == '__main__':
	main()

