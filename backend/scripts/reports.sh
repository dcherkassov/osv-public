# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


#!/bin/bash

. $WORKSPACE/../scripts/common.sh

REP_LOGGEN="/home/jenkins/scripts/loggen/loggen.py"
REP_DIR="/home/jenkins/logs/logruns/"
OF_ROOT="/home/jenkins/overlays/"
REP_GEN="/home/jenkins/scripts/loggen/gentexml.py"

TESTLOG="/home/jenkins/logs/${JOB_NAME}/testlogs/${Device}.${BUILD_ID}.${BUILD_NUMBER}.log"
TEST_RES="/home/jenkins/logs/${JOB_NAME}/testlogs/${Device}.${BUILD_ID}.${BUILD_NUMBER}.res.json"
GEN_TESTRES_FILE=""

function check_create_logrun () {

    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${Device}.$LOGRUN_FILE
        
        if [ ! -e $REP_LOGRUN_FILE ]
        then
            echo "creating $REP_LOGRUN_FILE logrun file"

            run_python $REP_LOGGEN --create-logrun $REP_LOGRUN_FILE --testplan $BATCH_TESTPLAN --board ${Device} --run-num $BUILD_NUMBER
        fi

        run_python $REP_LOGGEN --logrun-file $REP_LOGRUN_FILE --append-logfile $TEST_RES  --testname ${JOB_NAME}
    fi
}

function check_create_functional_logrun () {

    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${Device}.$LOGRUN_FILE
        
        if [ ! -e $REP_LOGRUN_FILE ]
        then
            echo "creating $REP_LOGRUN_FILE logrun file"

            run_python $REP_LOGGEN --create-logrun $REP_LOGRUN_FILE --testplan $BATCH_TESTPLAN --board ${Device} --run-num $BUILD_NUMBER
        fi

        run_python $REP_LOGGEN --append-funcres "$1" --logrun-file $REP_LOGRUN_FILE --testname ${JOB_NAME}
    fi
}


function set_testres_file () {
   if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        GEN_TESTRES_FILE=$TEST_RES
   fi
}

function gen_report() {
    if [ "$BATCH_TESTPLAN" ] && [ "$LOGRUN_FILE" ]
    then
        REP_LOGRUN_FILE=$REP_DIR/${Device}.$LOGRUN_FILE
        REPORT_XML=$REP_LOGRUN_FILE.xml

        run_python $REP_GEN --logrun-file $REP_LOGRUN_FILE --output-file $REPORT_XML
        texml $REPORT_XML $REPORT_XML.tex
        
        mkdir -p pdf_reports

        cd pdf_reports
        cp $REPORT_XML.tex .

        pdflatex $REPORT_XML.tex
        
    else
        echo "Omitting report generation"
    fi
}

