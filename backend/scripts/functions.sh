# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# These are supporting functions for test suites building process.
. $WORKSPACE/../scripts/params.sh
. $WORKSPACE/../scripts/common.sh

# Unpacks $tarball_path/$tarball into current directory.
# $1 - optional flag; if set to "nostrip",
#      the leading path components won't be stripped
function unpack {
  [ "$1" = "nostrip" ] && strip_opt= || strip_opt="--strip-components=1"

  case ${tarball/*./} in
    gz|tgz) key=z ;;
    bz2) key=j ;;
    tar) key= ;;
    *) echo "Unknown $tarball file format. Not unpacking."; return;; 
  esac

  tar ${key}xf $TEST_HOME/$tarball $strip_opt 
}

function is_empty {
# $1 - parameter

 if [ -z $1 ]; then
   echo "ERROR: EMPTY PARAMETER"
   exit
 fi
}

function get {
  case "$TRANSPORT" in
  "ssh")
    $SCP $LOGIN@${DEVICE}:"$1" "${*:2}"
    ;;
  "serial")
    ;;
  *)
   abort_job "Error reason: TRANSPORT"
   ;;
  esac
}

function put {
  case "$TRANSPORT" in
  "ssh")
    $SCP "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
    ;;
  "serial")
    ;;
  *)
   abort_job "Error reason: TRANSPORT"
   ;;
  esac  
}

# These are supporting functions for target command running
# TODO: Add descriptions for parameters in every function
function cmd {
  case "$TRANSPORT" in
  "ssh")
    ${SSH}${DEVICE} "$@"
    ;;
  "serial")
    ;;
  *)
   abort_job "Error reason: TRANSPORT"
   ;;
  esac
}

function safe_cmd {
# $1 - ?

  case "$ROOTFS_OOM" in
  "default")
   cmd "echo 15 > /proc/\$\$/oom_adj" || abort_job "Unable to setup OOM adj."
   ;;
  *)
   abort_job "Error reason: ROOTFS_OOM command not defined"
   ;;
  esac

  cmd "$@"
}

function report {
# $1 - remote shell command, $2 - test log file.
# XXX:$2 this parameter could be optional, by default we can use $TESTDIR/$TESTDIR.log

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $OSV_HOME/osv.$TESTDIR/$TESTDIR.log; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $2; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  fi
}

function report_append {
# $1 - remote shell command, $2 - test log file.

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $OSV_HOME/osv.$TESTDIR/$TESTDIR.log; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $2; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  fi
}

function abort_job {
# $1 - Abort reason string

  set +x
  echo -e "\n*** ABORTED ***\n"
  [ -n "$1" ] && echo -e "OSV error reason: $1\n"

  wget -qO- ${BUILD_URL}/stop > /dev/null
  while true; do sleep 5; done
}

function dump_syslogs {
# 1 - tarball template, 2 - before/after

  is_empty $1
  is_empty $2

  # We create /tmp/${2} dir in any case to capture target logs and prevent
  # log dump to $OSV_HOME dir.

  case "$ROOTFS_LOGREAD" in
    "default")
      cmd "mkdir -p /tmp/osv.${1} && cd /tmp/osv.${1} && /sbin/logread > ${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.$2" || abort_job "Error while ROOTFS_LOGREAD command execution on target"
      ;;
    "cat_messages")
      cmd "mkdir -p /tmp/osv.${1} && cd /tmp/osv.${1} && cat /var/log/messages > ${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.$2" || abort_job "Error while ROOTFS_LOGREAD command execution on target"
      ;;
    *)
      abort_job "ROOTFS_LOGREAD command not defined"
      ;;
  esac
}

function concurrent_check {
  LOCKFILE="$WORKSPACE/$TRIPLET.build.lock" 

  if [ -e ${LOCKFILE} ]; then

    while $(wget -qO- "$(cat ${LOCKFILE})/api/xml?xpath=*/building/text%28%29") && [ ! -e test_suite_ready ]
    do
      sleep 5
    done
  fi

  echo "${BUILD_URL}" > ${LOCKFILE}
}

# Wait for other builds of the same test running in parallel,
# process Rebuild flag, and unpack test sources if necessary.
# Returns 0 if actual build needs to be performed; 1 - otherwise.
# Build scripts must call this function in the beginning.
# $1 is passed directly to unpack().
function pre_build {
  source $WORKSPACE/../scripts/tools.sh

  mkdir -p $TRIPLET && cd $TRIPLET
  if concurrent_check; then
    [ "$Rebuild" = "true" ] && rm -rf *

    if [ ! -e test_suite_ready ]; then
      unpack $1
      return 0
    fi
  fi
  return 1
}

function build {
  pre_build $1 && test_build || return 1
  post_build
}

function post_build {
  true
}

function deploy {
  pre_deploy
  test_deploy
  post_deploy
}

function pre_deploy {
  cd "$WORKSPACE"
  if [ ! -e "$TRIPLET" ]; then
    if build; then 
      echo "Test builded(looks like first time)"
      cd $WORKSPACE/$TRIPLET
    else
      echo -e "Error reason: unable to change dir to $TRIPLET"
    fi
  else
    cd  $TRIPLET
  fi
}

function post_deploy {
  rm -f $LOCKFILE
}

function firmware {
  case "$ROOTFS_FWVER" in
  "default")
    FW=`cmd "uname -r | xargs echo "` || abort_job "Unable to get firmware version"
    ;;
  *)
    abort_job "Error reason: ROOTFS_FWVER command not defined"
    ;;
  esac
  export FWVER="$FW"
}

function pre_test {
# $1 - tarball template
# Make sure the target is alive, and prepare workspace for the test

  export SSHPASS=$PASSWORD

  is_empty $1

# Target cleanup flag check
  [ "$Target_Cleanup" = "true" ] && target_cleanup $1 || true

  cmd "true" || abort_job "Cannot connect to $DEVICE via $TRANSPORT"

# It is needed to create directory for test logs and system logs
  mkdir -p $WORKSPACE/../logs/$JOB_NAME/testlogs
  mkdir -p $WORKSPACE/../logs/$JOB_NAME/systemlogs

  # /tmp/${1} is needed to save logs on different partition

# Get target device firmware.
  firmware
  cmd "echo \"Firmware revision:\" $FWVER" || abort_job "Error while ROOTFS_FWVER command execution on target"

# XXX: Sync date/time between target device and framework host
# Also log memory and disk status as well as non-kernel processes,and interrupts

  case "$ROOTFS_STATE" in
  "default")
    cmd "echo; uptime; echo; free; echo; df -h; echo; mount; echo; ps |grep -Fv '  ['; echo; cat /proc/interrupts; echo" || abort_job "Error while  ROOTFS_STATE command execution on target"
    ;;
  *)
    abort_job  "Error reason: ROOTFS_STATE command not defined"
    ;;
  esac

  cmd "rm -rf $OSV_HOME/osv.$1 /tmp/$1; mkdir -p $OSV_HOME/osv.$1 /tmp/osv.$1" || abort_job "Could not create $1 and /tmp/$1 on $DEVICE"

# Log test name
  case "$ROOTFS_LOGGER" in
  "default")
    cmd "logger \"Starting test ${JOB_NAME}\"" || abort_job "Could not execute ROOTFS_LOGGER command"
    ;;
  *)
    abort_job "Error reason: ROOTFS_LOGGER command not defined"
    ;;
  esac

  dump_syslogs $1 "before"

# flush buffers to physical media and drop filesystem caches to make system load more predictable during test execution
  case "$ROOTFS_SYNC" in
  "default")
    cmd "sync" || abort_job "Unable to flush buffers on target"
    ;;
  *)
    abort_job "Error reason: ROOTFS_SYNC command not defined"
    ;;
  esac

  case "$ROOTFS_DROP_CACHES" in
  "default")
    cmd "echo 3 > /proc/sys/vm/drop_caches" || abort_job "Unable to drop filesystem caches"
    ;;
  *)
    abort_job "Error reason: ROOTFS_DROP_CACHES command not defined"
    ;;
  esac
}

function bench_processing {
  firmware
  export DEVICE=$DEVICE
  export GEN_TESTRES_FILE=$GEN_TESTRES_FILE

  echo -e "\n RESULT ANALYSIS \n"

  # Get the test results
  get_testlog $TESTDIR $OSV_HOME/osv.$TESTDIR/$TESTDIR.log
  DATA_FILE=/home/jenkins/logs/${JOB_NAME}/plot.data
  REF_FILE=/home/jenkins/tests/${JOB_NAME}/reference.log
  PYTHON_ARGS="-W ignore::DeprecationWarning -W ignore::UserWarning"
  # The first command checks thresholds, and exits with appropriate return code.
  # Jenkins aborts script execution on any failure, but the second command needs to be executed in any case, and after the first one.
  # Therefore, this trick with 'rc' variable is required to always execute both commands, and pass proper status to Jenkins at the same time.
  run_python $PYTHON_ARGS $WORKSPACE/../tests/${JOB_NAME}/parser.py $JOB_NAME $PLATFORM $BUILD_ID $BUILD_NUMBER $FW $PLATFORM $NODE_NAME && rc=0 || rc=1
  run_python $PYTHON_ARGS $WORKSPACE/../scripts/parser/dataload.py $JOB_NAME $DATA_FILE $REF_FILE
  if [ $rc -eq 1 ]; then
    false
  else 
    true
  fi
}

function post_test {
  # source generated prolog.sh file since post_test is called separately
  source /home/jenkins/work/prolog.sh
  export SSHPASS=$PASSWORD

  # re-source params to set correct DEVICE, LOGIN, SSH vars
  source $WORKSPACE/../scripts/params.sh

# $1 - tarball template, $2,$3,$4 - optional process names to kill

  is_empty $1

# Kill any stale processes if requested to do so.
# First, issue normal kill, and finally, if stale process was found, force its termination with signal 9.
  case "$ROOTFS_KILL" in
  "default")
    [ -n "$2" ] && cmd "pkill $2 && sleep 2 && pkill -9 $2; true"
    [ -n "$3" ] && cmd "pkill $3 && sleep 2 && pkill -9 $3; true"
    [ -n "$4" ] && cmd "pkill $4 && sleep 2 && pkill -9 $4; true"
    ;;
  *)
    abort_job "Error reason: ROOTFS_KILL command not defined"
    ;;
  esac

# Syslog dump
  dump_syslogs $1 "after"

# Get syslogs
  get /tmp/osv.${1}/*.${BUILD_ID}.* ${WORKSPACE}/../logs/${JOB_NAME}/systemlogs/

# Remove work and log dirs
  cmd "rm -rf $OSV_HOME/osv.$1 /tmp/osv.$1"

# log test completion message.
  cmd "logger \"Test $1 is finished\""

# Syslog comparison
  syslog_cmp $1
}

function target_cleanup {
  cmd "rm -rf $OSV_HOME/* /tmp/* $OSV_HOME/.local"
}

function target_reboot {
  case "$ROOTFS_REBOOT" in
  "default")
   cmd "/sbin/reboot &"
   ;;
  *)
   abort_job "Error reason: ROOTFS_REBOOT command not defined"
   ;;
  esac

  sleep 30 # This magic number required as we need to wait until device reboots
  cmd "true"
  if [ $? ]; then
   true
  else
   false
  fi
}

# $1 - tarball template
function build_cleanup {
 rm -rf ${1}-${PLATFORM}
}

function log_compare {
# 1 - tarball template, 2 - number of results, 3 - Criteria, 4 - n/p (i.e. negative or possitive)

  cd "${WORKSPACE}/../logs/${JOB_NAME}/testlogs"
  LOGFILE="${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log"
  PARSED_LOGFILE="${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.{4}.log"

  if [ -e $LOGFILE ]; then
    current_count=`cat $LOGFILE | grep -E "${3}" 2>&1 | wc -l`
    if [ $current_count -eq $2 ];then
      cat $LOGFILE | grep -E "${3}" | tee "$PARSED_LOGFILE"
      local TMP_P=`diff -u ${WORKSPACE}/../ref_logs/${JOB_NAME}/${1}_${4}.log "$PARSED_LOGFILE" 2>&1`  
      if [ $? -ne 0 ];then 
        echo -e "\nOSV error reason: Unexpected test log output:\n$TMP_P\n"
        check_create_functional_logrun "test error"
        false
      else
        check_create_functional_logrun "passed"
        true
      fi
    else 
      echo -e "\nOSV error reason: Mismatch in expected ($2) and actual ($current_count) pos/neg ($4) results. (pattern: $3)\n"
      check_create_functional_logrun "failed"
      false
    fi
  else 
    echo -e "\nOSV error reason: 'logs/${JOB_NAME}/testlogs/$LOGFILE' is missing.\n"
    check_create_functional_logrun "test error"
    false
  fi

  cd -
}

function get_testlog {
# $1 - tarball template,  $2 - full path to logfile
# XXX: It will be unified
  if [ -n "$2" ]; then
    get ${2} ${WORKSPACE}/../logs/${JOB_NAME}/testlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log
  else
    get $OSV_HOME/osv.$1/$1.log ${WORKSPACE}/../logs/${JOB_NAME}/testlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log
  fi;
}

function syslog_cmp {
# $1 - TESTDIR
  PREFIX="${WORKSPACE}/../logs/${JOB_NAME}/systemlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}"
  rc=0
  if [ -f ${PREFIX}.before ]; then
    if diff -ua ${PREFIX}.before ${PREFIX}.after | grep -vEf "$WORKSPACE/../scripts/syslog.ignore" | grep -E -e '\.(Bug:|Oops)'; then
      rc=1
    fi
  # else # special case for "reboot" test
    # if grep -vE -e '\.(info|notice|debug|warn)|Boot Reason: Warm' -f "$WORKSPACE/../scripts/syslog.ignore" ${PREFIX}.after; then
    #   rc=1
    # fi
  fi
  [ $rc -eq 1 ] && echo -e "\nOSV error reason: Unexpected syslog messages.\n"
  return $rc
}

# check is variable is set and fail if otherwise
function assert_define () {
    varname=$1
    if [ -z "${!varname}" ]
    then
        abort_job "$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

# check is variable is set and fail if otherwise
function check_capability () {
    varname=CAP_$1
    if [ -z "${!varname}" ]
    then
        abort_job "CAP_$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

function hd_test_mount_prepare () {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ] 
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV || /bin/true"
        cmd "mount $HD_MOUNT_BLOCKDEV $HD_MOUNT_POINT"
    fi

    cmd "mkdir -p $HD_MOUNT_POINT/osv.$TESTDIR"
 
}

function hd_test_clean_umount() {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    cmd "rm -rf $HD_MOUNT_POINT/osv.$TESTDIR"

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ] 
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV"
    fi
}
