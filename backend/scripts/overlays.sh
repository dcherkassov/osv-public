# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


. $WORKSPACE/../scripts/common.sh

OF_ROOT=/home/jenkins/overlays/
OF_CLASSDIR="$OF_ROOT/base"
OF_DEFAULT_SPECDIR=$OF_ROOT/test_specs/
OF_OVFILES=""

OF_CLASSDIR_ARGS="--classdir $OF_CLASSDIR"
OF_OVFILES_ARGS=""
OF_TESTPLAN_ARGS=""
OF_SPECDIR_ARGS="--specdir $OF_DEFAULT_SPECDIR"
OF_OUTPUT_FILE="/home/jenkins/work/prolog.sh"
OF_OUTPUT_FILE_ARGS="--output $OF_OUTPUT_FILE"

OF_OVGEN="/home/jenkins/scripts/ovgen/ovgen.py"

function set_overlay_vars() {
    echo "board overlay: $BOARD_OVERLAY"

    if [ "$BOARD_OVERLAY" ]
    then
        echo "using $BOARD_OVERLAY board overlay"

        OF_BOARD_FILE="$OF_ROOT/$BOARD_OVERLAY"

        if [ ! -f $OF_BOARD_FILE ]
        then
            abort_job "$OF_BOARD_FILE does not exist"
        fi

        OF_OVFILES_ARGS="--ovfiles $OF_BOARD_FILE"
    else
        abort_job "BOARD_OVERLAY is not defined"
    fi
    

    # prefer batch testplan over test-specific testplan
    if [ "$BATCH_TESTPLAN" ]
    then
        echo "using $BATCH_TESTPLAN batch testplan"
        OF_TESTPLAN="$OF_ROOT/$BATCH_TESTPLAN"

        if [ ! -f $OF_TESTPLAN ]
        then
            abort_job "$OF_TESTPLAN does not exist"
        fi

        OF_TESTPLAN_ARGS="--testplan $OF_TESTPLAN"
        
    elif [ "$TESTPLAN" ]
    then
        echo "BATCH_TESTPLAN is not sent, using $TESTPLAN testplan"
        OF_TESTPLAN="$OF_ROOT/$TESTPLAN"

        if [ ! -f $OF_TESTPLAN ]
        then
            abort_job "$OF_TESTPLAN does not exist"
        fi

        OF_TESTPLAN_ARGS="--testplan $OF_TESTPLAN"
    fi

    rm -f $OF_OUTPUT_FILE

    run_python $OF_OVGEN $OF_CLASSDIR_ARGS $OF_OVFILES_ARGS $OF_TESTPLAN_ARGS $OF_SPECDIR_ARGS $OF_OUTPUT_FILE_ARGS

    source $OF_OUTPUT_FILE
}

