# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os
import glob
import re
import argparse
import sys
import json
import string

log_lvl = 1

def debug_print(string, lev=1):
    if lev <= log_lvl:
        print "log: " + string

def append_logfile(logrun, logfile, testName):
    with open(logrun, "r+") as f:
        jd = json.load(f)
        rl = jd["runLogs"]
        ne = {"logFile": logfile, "testName": testName}
        rl.append(ne)
        debug_print ("appended logfile: %s" % ne)
        f.seek(0)
        f.truncate()
        
        f.write(json.dumps(jd, indent=4))

def append_funcres(logrun, res, testName):
    with open(logrun, "r+") as f:
        jd = json.load(f)
        rl = jd["runLogs"]
        ne = {"testName": testName, "testResult": res}
        rl.append(ne)
        debug_print ("appended functional result: %s" % ne)
        f.seek(0)
        f.truncate()
        
        f.write(json.dumps(jd, indent=4))


def create_logrun_file(logrun_file, testplan, device, run_num):
    with open(logrun_file, "w+") as f:
        jd = {"device": device, "testplan": testplan, "runLogs": [], "runNumber": run_num}
        debug_print("created logrun file: %s" % (jd))
        f.write(json.dumps(jd, indent=4))
        

def run(test_args=None):
    parser = argparse.ArgumentParser(description='Generate test run files')

    parser.add_argument('--create-logrun', help='create logrun file')
    parser.add_argument('--logrun-file', help='log run file for updating')
    parser.add_argument('--testname', help='name of test was run')
    parser.add_argument('--run-num', help='testrun number')
    parser.add_argument('--debug', help='{1,2,3} debug level (default is no debugging)', type=int)


    parser.add_argument('--append-logfile', help='logfile to append to logrun')
    parser.add_argument('--append-funcres', help='append functional test result')
    parser.add_argument('--testplan', help='testplan path')
    parser.add_argument('--board', help='name of board the tests are run on')

    args = parser.parse_args(args=test_args)

    if args.debug:
        if args.debug < 1 or args.debug > 3:
            print "Error: wrong debug lvl: %s" % (args.debug)
            sys.exit
        global log_lvl    
        log_lvl = args.debug

    if args.append_logfile:
        if (args.logrun_file == None) or (args.testname == None):
            print "--append-logfile needs logrun-file and testname arguments are specified\n"
            sys.exit(1)
            
        append_logfile(args.logrun_file, args.append_logfile, args.testname)

    elif args.append_funcres:
        if (args.logrun_file == None) or (args.testname == None):
            print "--append-funcres needs logrun-file and testname arguments are specified\n"
            sys.exit(1)
            
        append_funcres(args.logrun_file, args.append_funcres, args.testname)

    elif args.create_logrun:
        if (args.testplan == None) or (args.board == None) or (args.run_num == None):
            print "--create-logrun needs board, run-num and testplan arguments\n"
            sys.exit(1)

        create_logrun_file(args.create_logrun, args.testplan, args.board, args.run_num)
    else:
        print "Unkown command line: %s" % (args)
        sys.exit(1)


def testrun():
    # test_args = "--create-logrun tprun.json --board minnow --testplan testplan_sata.json".split()
    # run(test_args)

    # test_args = "--append-logfile minnow.2014-03-25_04-19-25.38.log --logrun-file tprun.json".split()

    test_args = "--append-funcres passed --logrun-file minnow-logrun.json --testname Functional.bzip2".split()
    
    run(test_args)

# testrun()
run()
