# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os
import glob
import re
import argparse
import sys
import json
import string

log_lvl = 0

# defines
OFVAR_NAME="NAME"
OFVAR_DESCRIPTION="DESCRIPTION"
LVAR_NAME="NAME"
LVAR_DESCRIPTION="DESCRIPTION"


class OFClass:
    def __init__(self):
        self.name=""
        self.description=""
        self.funcs = {}
        self.vars = {}
        self.cap_list=[]
        
    def __repr__(self):
        return "<OFClass name:\"%s\" descr:\"%s\" vars:%s funcs:%s>\n" % (self.name, self.description, self.vars, self.funcs)

    def __str__(self):
        return "<OFClass name:\"%s\" descr:\"%s\" vars:%s funcs:%s>\n" % (self.name, self.description, self.vars, self.funcs)

class OFLayer:
    def __init__(self):
        self.name=""
        self.description=""

        self.vars = {}
        
    def __repr__(self):
        return "<OFLayer name:\"%s\" descr:\"%s\" vars:%s>\n" % (self.name, self.description, self.vars)

    def __str__(self):
        return "<OFLayer name:\"%s\" descr:\"%s\" vars:%s>\n" % (self.name, self.description, self.vars)


class TestSpecs:
    def __init__(self):
        self.testName=""
        self.specList={}
        
    def __repr__(self):
        return "<TestSpecs name:\"%s\" descr:\"%s\" vars:%s>\n" % (self.testName, self.specs)

    def __str__(self):
        return "<TestSpecs name:\"%s\" descr:\"%s\" vars:%s>\n" % (self.testName, self.specs)

class OFParseException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)
        
class OFClassNotFoundException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

class OFVarNotFoundException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

class OFFunParseException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

class SpecException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)


def debug_print(string, lev=1):
    if lev <= log_lvl:
        print "log: " + string

# parse Overlay Framework variables definitions
def parseOFVars(line, ofc):
    m = re.search("OF\.(\w+)=\"(.*)\"", line)
    if m==None: return False

    var = m.group(1)
    val = m.group(2)

    debug_print ("OF var: %s = %s" % (var, val))

    if var == OFVAR_NAME:
        ofc.name = val
    elif var == OFVAR_DESCRIPTION:
        ofc.description = val
    
    return True


# parse variables definitions
def parseVars(line, ofc):
    m = re.search("(\w+)=\"(.*)\"", line)
    if m==None:
        debug_print("discarding string %s" % (line), 3)
        return False

    var = m.group(1)
    val = m.group(2)

    debug_print ("var: %s = %s" % (var, val))

    ofc.vars[var] = val

    return True

def parseFunctionBody(name, f):
    nestLevel = 1
    funcbody = ""

    while nestLevel != 0:
        nc = f.read(1)
        if nc == "{":
            nestLevel += 1
        elif nc == "}":
            nestLevel -= 1
        elif nc == "":
            raise OFFunParseException("EOF encountered while parsing body of %s" % (name))

        funcbody += nc

    return funcbody


def parseFunction(line, f):
    m = re.search("\s?function (\w+).*{", line)
    if m==None:
        debug_print("discarding string %s" % (line), 3)
        return False
        
    name = m.group(1)
    debug_print("parsed function %s" % name)

    funcbody = parseFunctionBody(name, f)
    funcbody = line + funcbody

    # TODO: add case for single-line functions

    debug_print ("body: %s" % (funcbody), 3)
        
    return (name, funcbody)

def baseParseFunction(line, f, ofc):
    fun = parseFunction(line, f)
    if fun:
        name, body = fun
        ofc.funcs[name] = body
        return True
    else:
        return False

# parse base file definitions
def parseBaseFile(baseFilePath, ofc):
    debug_print ("\n------------\nparsing " + baseFilePath + " osvclass ...\n")
    f = open(baseFilePath)
    while 1:
        line = f.readline()
        if not line: break

        if parseOFVars(line, ofc):
            debug_print("OF value found", 3)

        elif parseVars(line, ofc):
            debug_print("value found", 3)

        elif baseParseFunction(line, f, ofc):
            debug_print("function found", 3)


# parse all base files in dir
def parseBaseDir(baseDirPath, ofcls):
    debug_print ("\n------------\nparsing " + baseDirPath + " osvclass dir ...\n")
    osvFiles = glob.glob(baseDirPath + "/*.osvclass")

    for f in osvFiles:
        ofc = OFClass()
        parseBaseFile(f, ofc)
        debug_print ("parsed %s class\n------------\n" % (ofc.name))
        ofcls[ofc.name]=(ofc)


def parseInherit(line, ofcls):
    m = re.search("inherit \"(.+)\"", line)
    if m==None: return None
    clname = m.group(1)
    
    
    if clname not in ofcls:
        raise OFClassNotFoundException("No such class: %s" % (clname))

    return ofcls[clname]

def parseInclude(line, ofcls):
    m = re.search("include \"(.+)\"", line)
    if m==None: return None
    clname = m.group(1)
    
    if clname not in ofcls:
        raise OFClassNotFoundException("No such class: %s" % (clname))

    return ofcls[clname]

def parseLayerVarOverride(line, layer, inhclass):
    m = re.search("override (\w+) \"(.*)\"", line)
    if m==None:  return False

    var = m.group(1)
    val = m.group(2)

    debug_print ("overriding var: %s = %s" % (var, val))

    if var not in inhclass.vars:
        raise OFVarNotFoundException("variable %s is not found in %s class" % (var, inhclass.name))

    inhclass.vars[var] = val

    return True


def parseLayerFuncOverride(line, layer, inhclass, f):
    m = re.search("override-func (\w+)(.*)", line)
    if m==None: return False

    name = m.group(1)
    rest = m.group(2)

    debug_print ("overriding func: %s" % (name))
    nline = "function %s" % (rest)

    rv = parseFunctionBody(name, f)
    if rv:
        body = "function " + name + " " + rest + "\n" + rv
        inhclass.funcs[name] = body
    else:
        raise OFFunParseException("Cannot read %s function override" % (name))

    return True

# parse variables definitions
def parseLayerVarDefinition(line, layer, inhclass):
    m = re.search("(\w+)=\"(.*)\"", line)
    if m==None:
        debug_print("discarding string %s" % (line), 3)
        return False

    var = m.group(1)
    val = m.group(2)

    debug_print ("var: %s = %s" % (var, val))

    inhclass.vars[var] = val

    return True

def parseLayerCapList(line, layer, inhclass):
    m = re.search("BOARD\.CAP_LIST=\"(.*)\"", line)
    if m==None: return False

    caps = m.group(1)

    cap_list = caps.replace(" ", "").replace("\t", "").upper().split(",")

    if not cap_list:
        raise ParseException(("Caps list is empty: %s" % (line)))

    inhclass.cap_list=inhclass.cap_list+cap_list
        
    debug_print ("found board capabilities list: %s" % (caps), 0)

    return True

    
def parseOverrideFile(overrideFile, layer, ofcls):
    debug_print ("\n-----------\nparsing %s override ...\n" % (overrideFile))
    f = open(overrideFile)

    inheritClass = None # inherited class
    classes = [] # all classes

    # read inherit directive
    while 1:
        line = f.readline()
        if not line: break
        debug_print(line)
        
        ic = parseInherit(line, ofcls)
        if ic: # parsed line is inherit
            if inheritClass:
                raise ParseException("Can not inherit more than one base classes")
            else:
                inheritClass = ic
                classes.append(inheritClass)
        else:
            incClass = parseInclude(line, ofcls)
            if incClass:
                classes.append(incClass) # parsed line is include 
            elif inheritClass: # if parsed line is not include and we already have inherit, then we've parsed the prolog
                break;

    if inheritClass == None:
        print "%s does not contain inherit directive" 
        exit

    debug_print("All classes: %s" % (classes))
    debug_print("Inherited class: %s" % (inheritClass))

    while 1:
        if parseLayerCapList(line, layer, inheritClass):
            debug_print("Layer capability list found", 3)

        if parseLayerVarOverride(line, layer, inheritClass):
            debug_print("Layer var override found", 3)

        if parseLayerVarDefinition(line, layer, inheritClass):
            debug_print("Layer var definition found", 3)

        elif parseLayerFuncOverride(line, layer, inheritClass, f):
                debug_print("Layer func override found", 3)

        line = f.readline()
        if not line: break

    return classes

def generateProlog(outFilePath, ofcls, classes, tpFiles, specs):
    outfile = open(outFilePath, "w")
    
    for ofc in classes:
        # ofc = ofcls[name]
        name = ofc.name
        debug_print ("\nwriting %s base class" % (name))

        file.write(outfile, "#class: %s\n" % (name))
            
        for var in ofc.vars:
            outStr = "%s=\"%s\"" % (var, ofc.vars[var])
            outStr = outStr.replace('"', '\"')
            debug_print("%s <- %s" % (outFilePath, outStr))
            file.write(outfile, outStr+"\n")

        for cap in ofc.cap_list:
            outStr = "CAP_%s=\"yes\"" % (cap)
            debug_print("%s <- %s" % (outFilePath, outStr))
            file.write(outfile, outStr+"\n")

        file.write(outfile, "\n")

        for func in ofc.funcs:
            body = ofc.funcs[func]
            debug_print("%s <- %s()" % (outFilePath, func))
            file.write(outfile, body+"\n")

        file.write(outfile, "\n")

    if tpFiles != None:
        for tpf in tpFiles:
            parseGenTestPlan(tpf, outfile, outFilePath, specs)

def generateSpec(curTestSpecs, testName, specName, fout):
    del curTestSpecs["name"]
    for par in curTestSpecs:
        varname = "%s_%s" % (testName, par)
        varname = string.replace(varname, ".", "_").upper()
        value = "%s" % (curTestSpecs[par])
        outStr = "%s=%s" % (varname, value)
        
        debug_print (outStr, 3)
        fout.write(outStr + "\n")
    

def parseGenTestPlan(tpFilePath, fout, fname, specs):
    if specs == None:
        raise SpecException("Cannot generate testplan with empty specs")

    with open(tpFilePath) as f:
        debug_print("parsing `%s' TP" % (tpFilePath), 1)
        jd = json.load(f)
        name = jd["testPlanName"]
        fout.write("#testplan: %s\n" % (name))
        for t in jd["tests"]:
            testName = t["testName"]
            specName = t["spec"]

            if testName not in specs:
                raise SpecException("Cannot find test %s in spec list" % (testName))

            curTestSpecList = specs[testName]

            if specName not in curTestSpecList:
                raise SpecException("Cannot find spec %s in %s" % (specName, testName))

            curTestSpecs = curTestSpecList[specName]

            debug_print("generating spec %s for `%s'" % (specName, testName))
            generateSpec(curTestSpecs, testName, specName, fout)

        fout.write("\n")

def parseSpec(specFileName):
    ts = TestSpecs
    ts.specList={}

    debug_print("Parsing %s spec file" % (specFileName))

    with open(specFileName) as f:
        jd = json.load(f)
        name = jd["testName"]
        debug_print("parsing `%s' spec" % (name), 1)

        for spec in jd["specs"]:
            sn = spec["name"]
            ts.testName = name
            ts.specList[sn] = spec

    return ts


def parseSpecDir(specDir):
    specFiles = glob.glob(specDir + "/*.spec")
    tspList = {}
    
    debug_print ("Found follwowing %s spec files in %s" % (specFiles, specDir))

    for sf in specFiles:
        sp = parseSpec(sf)
        tspList[sp.testName] = sp.specList

    debug_print ("parsed specs: %s" % (tspList))

    return tspList
        
            
def run(test_args=None):
    parser = argparse.ArgumentParser(description='Read OF class files, override files and generate prolog defining all varibales and functions')

    parser.add_argument('--classdir', help='OF base class directory', required=True)
    parser.add_argument('--ovfiles', nargs='+', metavar='OVFILE', help='list of directories containing .override files', required=True)
    parser.add_argument('--testplans', nargs='+', metavar='TESTPLAN', help='list of test plan files', required=False)
    parser.add_argument('--output', help='output file name', required=True)
    parser.add_argument('--specdir', help='directory for test specializations', required=False)    
    parser.add_argument('--debug', help='{1,2,3} debug level (default is no debugging)', type=int, required=False)
    
    args = parser.parse_args(args=test_args)

    classdir = args.classdir
    ovfiles = args.ovfiles
    output = args.output


    if args.debug:
        
        if args.debug < 1 or args.debug > 3:
            print "Error: wrong debug lvl: %s" % (args.debug)
            sys.exit
            
        global log_lvl    
        log_lvl = args.debug

    testSpecs = {}

    if args.specdir != None:
        testSpecs = parseSpecDir(args.specdir)

    testPlans = {}
    if args.testplans != None:
        testPlans = args.testplans

    debug_print ("Using =%s, ovfiles=%s" % (classdir, ovfiles), 1)
    
    ofcls = {}
    parseBaseDir(classdir, ofcls)

    layers = {}
    for ovf in ovfiles:
        classes = parseOverrideFile(ovf, layers, ofcls)
        debug_print ("parsed %s override\n------------\n" % (ovf))

    generateProlog(output, ofcls, classes, testPlans, testSpecs)


def testrun():
    test_args =  "--classdir /home/dc/wrk/cogent/jenkins-install/backend/overlays/base/ --ovfiles /home/dc/wrk/cogent/jenkins-install/backend/overlays/boards/minnow.board --output prolog.sh --testplans /home/dc/wrk/cogent/jenkins-install/backend/overlays/testplans/testplan_mmc.json  --specdir /home/dc/wrk/cogent/jenkins-install/backend/overlays/test_specs --debug 2".split()
    # test_args =  "--classdir overlays-new/base/ --ovfiles overlays-new/boards/minnow.board --output prolog.sh  --debug 2".split()
    run(test_args)

run()
# testrun()

