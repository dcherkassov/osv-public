package hudson.plugins.logparser;

import java.util.HashMap;

public class LogParserDisplayConsts {
	
	final private HashMap<String,String> colorTable 			= new HashMap<String,String>();
	final private HashMap<String,String> iconTable 				= new HashMap<String,String>();
	final private HashMap<String,String> linkListDisplay 		= new HashMap<String,String>();
	final private HashMap<String,String> linkListDisplayPlural 	= new HashMap<String,String>();

	public LogParserDisplayConsts() {
		// Color of each status
		colorTable.put(LogParserConsts.ERROR, "red");
		colorTable.put(LogParserConsts.WARNING, "orange");
		colorTable.put(LogParserConsts.INFO, "green");
		colorTable.put(LogParserConsts.START, "blue");

		colorTable.put(LogParserConsts.REFERENCE_ERROR, "red");
		colorTable.put(LogParserConsts.REFERENCE_WARNING, "orange");
		colorTable.put(LogParserConsts.REFERENCE_INFO, "green");

		// Icon for each status in the summary
		iconTable.put(LogParserConsts.ERROR, "red.gif");
		iconTable.put(LogParserConsts.WARNING, "yellow.gif");
		iconTable.put(LogParserConsts.INFO, "green.gif");

		iconTable.put(LogParserConsts.REFERENCE_ERROR, "red.gif");
		iconTable.put(LogParserConsts.REFERENCE_WARNING, "yellow.gif");
		iconTable.put(LogParserConsts.REFERENCE_INFO, "green.gif");


		// How to display in link summary html
		linkListDisplay.put(LogParserConsts.ERROR, "Failed");
		linkListDisplay.put(LogParserConsts.WARNING, "Skipped");
		linkListDisplay.put(LogParserConsts.INFO, "Passed");

		linkListDisplay.put(LogParserConsts.REFERENCE_ERROR, "Failed (Reference)");
		linkListDisplay.put(LogParserConsts.REFERENCE_WARNING, "Skipped (Reference)");
		linkListDisplay.put(LogParserConsts.REFERENCE_INFO, "Passed (Reference)");


		linkListDisplayPlural.put(LogParserConsts.ERROR, "Fails");
		linkListDisplayPlural.put(LogParserConsts.WARNING, "Skips");
		linkListDisplayPlural.put(LogParserConsts.INFO, "Passes");


		linkListDisplayPlural.put(LogParserConsts.REFERENCE_ERROR, "Fails (Reference)");
		linkListDisplayPlural.put(LogParserConsts.REFERENCE_WARNING, "Skips (Reference)");
		linkListDisplayPlural.put(LogParserConsts.REFERENCE_INFO, "Passes (Reference)");
	}

	public HashMap<String,String> getColorTable() {
		return colorTable;
	}

	public HashMap<String,String> getIconTable() {
		return iconTable;
	}

	public HashMap<String,String> getLinkListDisplay() {
		return linkListDisplay;
	}

	public HashMap<String,String> getLinkListDisplayPlural() {
		return linkListDisplayPlural;
	}

}
