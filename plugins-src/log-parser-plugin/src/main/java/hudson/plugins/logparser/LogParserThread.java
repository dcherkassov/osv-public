package hudson.plugins.logparser;


import hudson.console.ConsoleNote;

import java.io.IOException;
import java.lang.NumberFormatException;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.logging.Logger;
import java.util.Stack;


class LogParserThread extends Thread {

	  private 		LogParserLogPart 	logPart;
	  private final String[] 	parsingRulesArray;
	  private final Pattern[] 	compiledPatterns;
	  private final	int 		threadNum;
	  private 		Stack[] 	logPartStatuses;
	  private 		int 		numOfLines;
	  private final LogParserReader logParserReader;

	  public LogParserThread(final LogParserReader logParserReader,final String[] parsingRulesArray, final Pattern[] compiledPatterns,final int threadNum) {
		  this.parsingRulesArray 	= parsingRulesArray;
		  this.compiledPatterns 	= compiledPatterns;
		  this.threadNum			= threadNum;
		  this.logParserReader 		= logParserReader;
	  }

	  public void run() {
		  try {
			// Synchronized method so as not to read from the same file from several threads.
			logPart = logParserReader.readLogPart(this.threadNum);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  logPartStatuses = getLineStatuses(logPart.getLines());
//		try {
//			logPartStatuses = channel.call(new Callable<String[],RuntimeException>(){
//			    public String[] call() {
//			    	return getLineStatuses(logPart.getLines());
//			    }
//			});
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (RuntimeException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}


	  }

	  public Stack[] getLineStatuses() {
		  return this.logPartStatuses;
	  }

	  public LogParserLogPart getLogPart() {
		  return this.logPart;
	  }

	  
	  public int getNumOfLines() {
		  return this.numOfLines;
	  }

	  private final Stack[] getLineStatuses(final String[] logPart){
		  	
		  	final Logger logger = Logger.getLogger(this.getClass().getName());
		  	logger.log(Level.INFO,"LogParserThread: Start parsing log part "+this.logPart.getLogPartNum());
		  	
		  	numOfLines = 0;
		  	int numOfChars = 0;
			//String[] result = new String[logPart.length];
            Stack[] result = new Stack[logPart.length];
			for (int i=0;i<logPart.length;i++) {
				final String line = logPart[i];
				if (line == null) {
					continue;
				}
				numOfLines++;
				numOfChars += line.length(); 
				final Stack status = getLineStatus(line);
				//final String status = getLineStatus(line); 
				result[i] = status;
			}
			
			logger.log(Level.INFO,"LogParserThread: Done parsing log part "+this.logPart.getLogPartNum());

			return result;
	  }

	  private Stack getLineStatus(String line) {
	      // For now, strip out ConsoleNote(s) before parsing.
	      // Notes are injected into log lines, and can break start-of-line patterns,
	      // and include html.  Will likely need alternative way to handle in the future.
          final Stack result = new Stack();
	      line = ConsoleNote.removeNotes(line);
	    	for (int i=0;i<this.parsingRulesArray.length;i++){
	    		final String parsingRule = this.parsingRulesArray[i];
                final Matcher matcher = this.compiledPatterns[i].matcher(line);
	    		if (!LogParserUtils.skipParsingRule(parsingRule) && 
	    			this.compiledPatterns[i] != null && matcher.find()) { 
		 	    		 final String status = parsingRule.split("\\s")[0];
                         try {
                            String parse_line = matcher.replaceAll("");
                            parse_line = parse_line.replace(" ", "");
                            parse_line = parse_line.replace("\t", "");
                            final int count = Integer.parseInt(parse_line);

                            result.push(count);
                            if (count == 0) {
                                result.push(LogParserConsts.NONE);
                            } else {
		    			        result.push(LogParserUtils.standardizeStatus(status));
                            }
                         } catch (NumberFormatException e) {
                            result.push(1);
		    			    result.push(LogParserUtils.standardizeStatus(status));
                         }
                         return result;
	    		}
	    	}

            result.push(0);
            result.push(LogParserConsts.NONE);

	    	return result;
	  }

	public int getThreadNum() {
		return threadNum;
	}



	  
}
