package hudson.plugins.plots;

import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.model.Result;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Publisher;
import hudson.tasks.Recorder;

import net.sf.json.JSONObject;

import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.DataBoundConstructor;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlotsRec extends Recorder {
	
		public final boolean osvp;

    @DataBoundConstructor
	  public PlotsRec () {
			this.osvp = true;
   	}
           
    private static final class AbortException extends RuntimeException {
    }

    public BuildStepDescriptor<Publisher> getDescriptor() {
        return DescriptorImpl.DESCRIPTOR;
    }
    
    public static final class DescriptorImpl extends BuildStepDescriptor<Publisher> {
	
        public static final DescriptorImpl DESCRIPTOR = new DescriptorImpl();

        private DescriptorImpl() {
            super(PlotsRec.class);
						load();
      	}   

        // The following string will be displayed on job configuration page. 
        public String getDisplayName() {
    			return "Show benchmark results plot";
        }

        public String getHelpFile() {
            return "/plugin/plots/help.html";
        }

        public boolean isApplicable(final Class<? extends AbstractProject> jobType) {
					return true;
        }
        
		@Override
		public boolean configure (final StaplerRequest req, final JSONObject json) throws FormException {	
	    return true;
		}

    }

	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE ;
	}
 
}

