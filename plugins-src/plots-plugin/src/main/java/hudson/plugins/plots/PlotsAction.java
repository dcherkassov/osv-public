package hudson.plugins.plots;

import hudson.model.ProminentProjectAction;

public abstract class PlotsAction implements ProminentProjectAction {

    public String getIconFileName() {
        return null;
    }

    public String getDisplayName() {
        return "Messages";
    }

    public String getUrlName() {
        return "Messages.UrlName()";
    }                               

}
