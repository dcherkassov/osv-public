/*
*
* This class is required by Jenkins to register new plug-in.
*
*/
package hudson.plugins.plots;

import hudson.Extension;
import hudson.Plugin;

public class PlotsPlugin extends Plugin {

    @Extension
    public static final PlotsRec.DescriptorImpl PLOTS_DESCRIPTOR = PlotsRec.DescriptorImpl.DESCRIPTOR;

    public static PlotsRec.DescriptorImpl getDescriptor() {
        return PLOTS_DESCRIPTOR;
    }

}