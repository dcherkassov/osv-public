package hudson.plugins.plots;

import hudson.plugins.plots.PlotsProperty.PlotsDescriptor;

import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Run;
import hudson.model.Hudson;

import hudson.util.DataSetBuilder;
import hudson.util.ShiftedCategoryAxis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;  

import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

public class PlotsProjectAction extends PlotsAction {
    
    AbstractProject<? extends AbstractProject, ? extends AbstractBuild> project;

    public PlotsProjectAction (AbstractProject<? extends AbstractProject, ? extends AbstractBuild> project) {
        this.project = project;
    }

    @Override
    public String getUrlName() {
        return "/plugin/plots";
    }
    // This method works
    public boolean getOSVP() {
			return false;
    }
		/** Shortcut for the jelly view */
    public boolean showPlot() {
    	return Hudson.getInstance().getDescriptorByType(PlotsDescriptor.class).isShowPlot();
    } 
}
