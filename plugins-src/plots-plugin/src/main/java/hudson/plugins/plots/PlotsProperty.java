package hudson.plugins.plots;

import hudson.model.AbstractProject;
import hudson.model.Action;
import hudson.model.Job;
import hudson.model.JobProperty;
import hudson.model.JobPropertyDescriptor;
import hudson.Extension;

import net.sf.json.JSONObject;
import org.kohsuke.stapler.StaplerRequest;

/**
 * This Property sets plugin action. 
 * 
 * @author dvrzalik
 */
public class PlotsProperty extends JobProperty<Job<?, ?>> {
    
    @Override
    public Action getJobAction(Job<?, ?> job) {
			return new PlotsProjectAction((AbstractProject) job);
    }
    
    @Extension
    public static final class PlotsDescriptor extends JobPropertyDescriptor {

        public PlotsDescriptor() {
					load();
					showPlot = true;
        }

        //Show plot on the project page?
        private boolean showPlot;

        @Override
        public String getDisplayName() {
					return "Messages.DisplayName()";
        }

        @Override
        public PlotsProperty newInstance (StaplerRequest req, JSONObject formData) throws FormException {
					return new PlotsProperty();
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException {
					//showPlot = req.getParameter("PlotsRec.osvp") != null;
					showPlot = true;
					save();
					return super.configure(req, formData);
        }

        @Override
        public boolean isApplicable(Class<? extends Job> jobType) {
					return true;
        }

        public boolean isShowPlot() {
            //The graph is shown by default
 	    			return showPlot;
        }

        public void setShowPlot(Boolean showPlot) {
            this.showPlot = showPlot;
        }
    }
}

    
