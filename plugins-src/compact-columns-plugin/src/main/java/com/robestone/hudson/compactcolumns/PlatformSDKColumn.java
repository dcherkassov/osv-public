/*
 * The MIT License
 * 
 * Copyright (c) 2011, Matt Ranostay
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.robestone.hudson.compactcolumns;

import hudson.Extension;
import hudson.model.Job;

import org.kohsuke.stapler.DataBoundConstructor;

import java.util.List;

public class PlatformSDKColumn extends AbstractCompactColumn {
    @DataBoundConstructor
    public PlatformSDKColumn() {
    }

    public String getPlatformSDKColumnSortData(Job<?, ?> job) {
    	List<BuildInfo> builds = getBuilds(job);
    	if (builds.isEmpty()) {
    		return "0";
    	}
    	BuildInfo latest = builds.get(0);
        StringBuffer hashString = new StringBuffer();

        hashString.append(latest.getPlatformSDK());
        hashString.append(latest.getRun().getDisplayName());
        hashString.append(String.valueOf(latest.getRun().getNumber()));

        return hashString.toString();

    }

    @Override
    protected boolean isFailedShownOnlyIfLast() {
        return false;
    }

    @Override
    protected boolean isUnstableShownOnlyIfLast() {
        return false;
    }

    @Extension
    public static class PlatformSDKColumnDescriptor extends
            AbstractCompactColumnDescriptor {
        @Override
        public String getDisplayName() {
            return Messages.Compact_Column_Platform_SDK();
       }
    }
}
