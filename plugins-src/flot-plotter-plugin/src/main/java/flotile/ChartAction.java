package flotile;
import java.io.*;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Action;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import hudson.util.DataSetBuilder;

public class ChartAction implements Action {
	private AbstractProject<?,?> project;

	public ChartAction(AbstractProject<?,?> project) {
		this.project = project;
	}
	
	public String getDisplayName() {
		return "Flotile";
	}

	public String getIconFileName() {
		//return "clipboard.gif";
		return null;
	}

	public String getUrlName() {
		return "flot";
	}
}
