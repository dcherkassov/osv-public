package flotile;

import java.io.*;
import java.util.*;
import hudson.*;
import hudson.tasks.*;
import hudson.model.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import java.io.IOException;

@SuppressWarnings("unchecked")
public class FlotPublisher extends Recorder {
	
	@DataBoundConstructor
	public FlotPublisher() {
		
	}
	
	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE;
	}
	
	@Override
    public Action getProjectAction(AbstractProject<?,?> project) {
        return new ChartAction(project);
    }
	
  @Override
		public boolean perform(AbstractBuild<?,?> build, Launcher launcher, BuildListener listener) throws InterruptedException, IOException {
	 			return true;	
		}

	@Extension
    public static final DescriptorImpl DESCRIPTOR = new DescriptorImpl();
    public static final class DescriptorImpl extends BuildStepDescriptor<Publisher> {
    	
    	public DescriptorImpl() {
    		super(FlotPublisher.class);
    		load();
    	}

    	@Override
    	public boolean isApplicable(Class<? extends AbstractProject> jobType) {
    		return true; // applicable to all project type
    	}

    	@Override
    	public String getDisplayName() {
    		return "Flot Publisher";
    	}
  	
    }
}
