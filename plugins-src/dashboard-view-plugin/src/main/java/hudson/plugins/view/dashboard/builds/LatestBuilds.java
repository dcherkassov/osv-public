package hudson.plugins.view.dashboard.builds;

import hudson.EnvVars;
import hudson.Extension;
import hudson.model.Descriptor;
import hudson.model.Job;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.plugins.view.dashboard.DashboardPortlet;
import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.kohsuke.stapler.DataBoundConstructor;

import hudson.plugins.view.dashboard.Messages;

public class LatestBuilds extends DashboardPortlet{

  /**
	 * Number of latest builds which will be displayed on the screen
	 */  
  private int numBuilds = 10;

	@DataBoundConstructor
	public LatestBuilds(String name, int numBuilds) {
		super(name);
    this.numBuilds = numBuilds;
	}

  public int getNumBuilds() {
    return numBuilds <= 0 ? 10 : numBuilds;
  }

  public String getPlatformSDK(Run run) {
    try {
        EnvVars env = run.getEnvironment(TaskListener.NULL);
        return env.get("Platform_SDK");
    }
    catch (Exception e) {
        return null;
    }
  }

  public String getPlatformSDKColumnSortData(Run build) {
    StringBuffer hashString = new StringBuffer();

    if (getPlatformSDK(build) != null)
        hashString.append(getPlatformSDK(build));

    hashString.append(build.getDisplayName());
    hashString.append(String.valueOf(build.getNumber()));

    return hashString.toString();
  }

  public String getTarget(Run run) {
    try {
        EnvVars env = run.getEnvironment(TaskListener.NULL);
        return env.get("Device").split("\\|")[0];
    }
    catch (Exception e) {
        return null;
    }
  }

  public String getTargetColumnSortData(Run build) {
    StringBuffer hashString = new StringBuffer();

    if (getTarget(build) != null)
        hashString.append(getTarget(build));

    hashString.append(build.getDisplayName());
    hashString.append(String.valueOf(build.getNumber()));

    return hashString.toString();
  }

  public String getTimestampSortData(Run run) {
    return String.valueOf(run.getTimeInMillis());
  } 

  public String getTimestampString(Run run) {
    return DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(new Date(run.getTimeInMillis()));
  }

	/**
	 * Last <code>N_LATEST_BUILDS</code> builds
	 *
	 */
	public List<Run> getFinishedBuilds() {
    List<Job> jobs = getDashboard().getJobs();
		List<Run> allBuilds = new ArrayList<Run>();
		for (Job job : jobs) {
      List<Run> builds = job.getBuilds();
      allBuilds.addAll(builds);
		}
		Collections.sort(allBuilds, Run.ORDER_BY_DATE);
		List<Run> recentBuilds = new ArrayList<Run>();
		if(allBuilds.size() < getNumBuilds())
			recentBuilds = allBuilds;
		else
			recentBuilds = allBuilds.subList(0,getNumBuilds());
			
		return recentBuilds;
	}
/*
    public String getDateString(Run run) {
        return DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date(run.getTimeInMillis()));
    }

    public List<Run> getBuildsByDate(List<Run> builds, String date) {
        List<Run> dateBuilds = new ArrayList<Run>();

        for (Run build : builds) {
            if (getDateString(build).equals(date))
                dateBuilds.add(build);
        }
        return dateBuilds;
    }
*/
    public String getBuildColumnSortData(Run<?, ?> build) {
        return String.valueOf(build.getNumber());
    }
/*
    public List<String> getDates(List<Run> builds) {
        List<String> dates = new ArrayList<String>();

        for (Run build: builds) {
            if (!dates.contains(getDateString(build)))
                dates.add(getDateString(build));
        }

        return dates;
    }
*/
	@Extension
    public static class DescriptorImpl extends Descriptor<DashboardPortlet> {

		@Override
		public String getDisplayName() {
			return Messages.Dashboard_LatestBuilds();
		}

	}
	
}
