#!/usr/bin/env python
import os
import sys

from plotting_module import generate_plots
from plotting_module.decorators import parse_platform_sdk

def main():
    filename = sys.argv[1]
    if not os.path.exists(filename):
        print >>sys.stderr, "Error: Filename '%s' doesn't exist. Aborting" % filename
        sys.exit(1)
    if os.path.getsize(filename) <= 0:
        print >>sys.stderr, "Error: Filename '%s' is empty. Aborting" % filename
        sys.exit(1)
    job_name = sys.argv[2]
    if not generate_plots.has_key(job_name):
        print >>sys.stderr, "Error: Cannot find parsing engine for test '%s'" % job_name
        sys.exit(2)
    platform_sdk = sys.argv[3]
    reference_job = sys.argv[4] in ("true", "1")

    if reference_job:
        platform_sdk = "".join((parse_platform_sdk(platform_sdk), " - Reference"))
    generate_plots[job_name](filename, platform_sdk)


def show_usage():
    print "./generate_plot.py filename job_name platform_sdk is_reference_job"

if __name__ == '__main__':
    argc = len(sys.argv)

    if argc != 5:
        show_usage()
    else:
        main()
