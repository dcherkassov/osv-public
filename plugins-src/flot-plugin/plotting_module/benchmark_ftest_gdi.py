#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.ftest-gdi"

def benchmark_ftest_gdi(filename, platform_sdk = ""):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    ftest_gdi_data = load_data(NAME + "/ftest_gdi.pickle", platform_sdk)

    if not lines:
        return
    line = lines[-1]
    line = line.strip()

    line = line[18:].split(" ")[0]

    if line.isdigit():
        ftest_gdi_data = add_value(ftest_gdi_data, line, "Milliseconds", platform_sdk)
 
    template = Template()
    template.set_platform_sdk(platform_sdk)

    template.make_plot("10,000 GDI Operation", ftest_gdi_data, verbose = 0) 

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/ftest_gdi.pickle", ftest_gdi_data, platform_sdk)
