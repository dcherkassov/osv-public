#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.OpenSSL"

def benchmark_openssl(filename, platform_sdk = ""):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    openssl_data = load_data(NAME + "/openssl.pickle", platform_sdk)

    for line in lines:
        if [ x for x in ("rsa", "dsa") if line.startswith(x) ]:
            items = [ x.strip() for x in line.split(" ") if x ]

            crypt = items[0]
            bits = items[1]
            sign = items[5]
            verify = items[6]

            openssl_data = add_value(openssl_data, sign, crypt, bits, "sign/sec", platform_sdk)
            openssl_data = add_value(openssl_data, verify, crypt, bits, "verify/sec", platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in [ "dsa", "rsa" ]:
        for j in string_list([ 512, 1024, 2048, 4096 ]):
            if i == "dsa" and j == '4096':
                continue # Plotting data doesn't exist
            template.make_plot("%s %s bit" % (i, j), openssl_data[i][j], ['sign/sec', 'verify/sec'])

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/openssl.pickle", openssl_data, platform_sdk)
