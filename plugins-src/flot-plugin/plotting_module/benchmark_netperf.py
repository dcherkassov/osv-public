#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.netperf"

def benchmark_netperf(filename, platform_sdk = ''):
    f = open(filename, "r")
    test_name = get_test_name(filename)
    lines = [ x.strip() for x in f.readlines() ]

    create_workspace(NAME)
    netperf_data = load_data(NAME + "/netperf.pickle", platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    if test_name == "tcp_range":
        idx = 0
        for line in lines:
            if line.startswith("./netperf"):
                line = [ i for i in lines[idx + 8].split(" ") if i ]
                name = " ".join((line[2],"byte message / mbytes sec"))
                value = line[4]
                netperf_data = add_value(netperf_data, value, test_name, name, platform_sdk)
            idx += 1
    if netperf_data.has_key("tcp_range"):
        template.make_plot("TCP Range Test 64K Socket Size", netperf_data["tcp_range"], verbose = 0)


    if test_name == "tcp_rr":
        idx = 0
        for line in lines:
            if line.startswith("./netperf"):
                line = [ i for i in lines[idx + 9].split(" ") if i ]
                if len(line) < 5:
                    continue
                name = "Request/Response Size: %s - %s bytes" % (line[2], line[3])
                value = line[5] 
                netperf_data = add_value(netperf_data, value, test_name, name, platform_sdk)
            idx += 1
    if netperf_data.has_key("tcp_rr"):
        template.make_plot("TCP RR (transaction/second) Test", netperf_data["tcp_rr"], verbose = 0)

    if test_name == "tcp_stream":
        idx = 0
        for line in lines:
            if line.startswith("./netperf"):
                line = [ i for i in lines[idx + 8].split(" ") if i ]
                socket_size = line[0]
                name = " ".join((line[2], "byte message / mbytes sec"))
                value = line[4]
                netperf_data = add_value(netperf_data, value, test_name, socket_size, name, platform_sdk)
            idx += 1
    if netperf_data.has_key("tcp_stream"):
        keys = netperf_data["tcp_stream"].keys()
        keys.sort()

        for i in keys:
            size = int(round(int(i) / 1024.0))
            template.make_plot("TCP Stream Test %sK Socket Size" % size, netperf_data["tcp_stream"][i], verbose = 0)

    # UDP RR TODO! Need actual results for this.

    if test_name == "udp_stream":
        idx = 0
        for line in lines:
            if line.startswith("./netperf"):
                line = [ i for i in lines[idx + 6].split(" ") if i ]
                name = " ".join((line[1], "byte message / mbytes sec"))
                value = line[5]
                netperf_data = add_value(netperf_data, value, test_name, name, platform_sdk)
            idx += 1
    if netperf_data.has_key("udp_stream"):
        template.make_plot("UDP Stream Test 64K Socket Size", netperf_data["udp_stream"], verbose = 0)


    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/netperf.pickle", netperf_data, platform_sdk)
