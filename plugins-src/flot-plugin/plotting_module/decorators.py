#vim: ts=4 expandtab

import os
import pickle

def create_workspace(NAME):
    if not os.path.isdir(NAME):
        os.mkdir(NAME)

    if not os.path.isdir(NAME + "/reports"):
        os.mkdir(NAME + "/reports")

    symlink = NAME + "/reports/flot"
    if not os.path.islink(symlink):
        os.symlink("../../flot", symlink)

def add_value(data, value, *values):
    dataset = data
    for i in values:
        if not dataset.has_key(i):
            dataset[i] = {}
        dataset = dataset[i]
    dataset["value"] = value
    return data


def parse_platform_sdk(platform_sdk):
    if platform_sdk:
        return platform_sdk.split("-")[0].strip()
    return ""


def load_data(filename, platform_sdk = ''):
    sdk = parse_platform_sdk(platform_sdk)
    filename = ".".join((filename, sdk))

    if not os.path.exists(filename):
        return {}
    return pickle.load(open(filename, "rb"))


def save_data(filename, data, platform_sdk = ''):
    sdk = parse_platform_sdk(platform_sdk)
    filename = ".".join((filename, sdk))

    pickle.dump(data, open(filename, "wb")) 

def string_list(values):
    return [ str(x) for x in values ]

def capitalize_string(label):
    return label.title()

def sort_list(values):
    max_size = 0
    for i in values:
        if len(i) > max_size:
            max_size = len(i)
    values = [ i.rjust(max_size) for i in values ]
    values.sort()

    return [ i.strip() for i in values ]

def get_test_name(filename):
    filename = os.path.split(filename)[-1]
    return filename.split(".")[1]

def clean_line(line):
    for i in range(1,len(line)):
        line[i] = line[i].split(" ")[0]
    line[0] = " ".join([ i for i in line[0].split(" ") if i])
    return line


