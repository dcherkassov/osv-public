#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.lmbench2"

items = {
    #0: "Basic system parameters",
    #1: "Processor and Processes",
    2: "Context switching",
    3: "Local communication latencies",
    4: "File and VM system latencies",
    5: "Local communication bandwidth",
    6: "Memory latencies",
} 

values = {
    2: { # Context switching
        0: "2 page 0K context switch/microsecs",
        1: "2 page 16K context switch/microsecs",
        2: "2 page 64K context switch/microsecs",
        3: "8 page 16K context switch/microsecs",
        4: "8 page 64K context switch/microsecs",
        5: "16 page 16K context switch/microsecs",
        6: "16 page 64K context switch/microsecs",
    },
    3: { # Local Communication latencies
        0: "2 page 0K context switch/microsecs",
        1: "Pipe latency/microsecs",
        2: "AF UNIX latency/microsecs",
        3: "UDP latency/microsecs",
        4: "TCP latency/microsecs",
        5: "TCP connection latency/microsecs",
    },
    4: { # File and VM system latencies
        0: "0Kb File Create/microsecs",
        1: "0Kb File Delete/microsecs",
        2: "10Kb File Create/microsecs",
        3: "10Kb File Delete/microsecs",
    },
    5: { # Local communication bandwidth
        0: "Pipe MB/s",
        1: "AF UNIX MB/s",
        2: "TCP MB/s",
        3: "File reread MB/s",
        4: "Mmap reread MB/s",
        5: "Bcopy (libc) MB/s",
        6: "Bcopy (hand) MB/s",
        7: "Mem read MB/s",
        8: "Mem write MB/s",
    },
    6: { # Memory latencies
        1: "L1 latency/nanosecs",
        2: "L2 latency/nanosecs",
    },
}

def benchmark_lmbench2(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    lmbench2_data = load_data(NAME + "/lmbench2.pickle", platform_sdk)

    idx = 0
    for line in lines:
        if sum([ line.startswith(i) for i in [ "\t", " ", "-", "Host" ] ]):
            continue
        line = line.strip()
        if not line:
            continue
        line = [ i for i in line.split(" ") if i ]
        if not "Linux" in line:
            continue
        if items.has_key(idx):
            line = line[3:]
            name = items[idx]
            for k, v in values[idx].items():
                lmbench2_data = add_value(lmbench2_data, line[k], name, v, platform_sdk)
        idx += 1

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in lmbench2_data.keys():
        template.make_plot("%s" % i, lmbench2_data[i], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/lmbench2.pickle", lmbench2_data, platform_sdk)
