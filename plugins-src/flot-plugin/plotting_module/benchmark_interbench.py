#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.Interbench"

def benchmark_interbench(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ x.strip() for x in f.readlines() if x ]

    create_workspace(NAME)
    interbench_data = load_data(NAME + "/interbench.pickle", platform_sdk)

    branch = ""
    for line in lines:
        if line.startswith("--- Benchmarking"):
            branch = ((line.split("of")[1]).split(" in")[0]).strip()
        else:
            if not branch:
                continue
            line = line.replace("\t"," ")
            line = line.split(" ")
            line = [ i.strip() for i in line if i ]
            if not line or line[0] == 'Load':
                continue
            name = line[0]
            latency = line[1]
            interbench_data = add_value(interbench_data, latency, branch, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in interbench_data.keys():
        template.make_plot("%s Benchmarking" % i, interbench_data[i], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/interbench.pickle", interbench_data, platform_sdk)
