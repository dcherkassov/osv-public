#vim: ts=4 expandtab

from template import *
from decorators import *

import sys

NAME = "Benchmark.GLMark"

def benchmark_glmark(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    glmark_data = load_data(NAME + "/glmark.pickle", platform_sdk)

    if len(lines) < 13: # not enough output. abort reporting.
        return

    report_group = ""
    for line in lines:
        if not line.startswith(" "):
           report_group = line.strip()
        else:
            line = line.strip()
            line = line.split("FPS: ")
            if not report_group or not len(line) > 1:
                continue
            name = line[0].strip() + " FPS"
            value = line[1]
            
            glmark_data = add_value(glmark_data, value, report_group, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    reports = glmark_data.keys()
    for report in reports:
        template.make_plot(report, glmark_data[report], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/glmark.pickle", glmark_data, platform_sdk)
