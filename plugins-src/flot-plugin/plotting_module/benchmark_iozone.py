#vim: ts=4 expandtab

from template import *
from decorators import *

import sys

NAME = "Benchmark.IOzone"

def benchmark_iozone(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ x.strip() for x in f.readlines() ]

    create_workspace(NAME)
    iozone_data = load_data(NAME + "/iozone.pickle", platform_sdk)

    try:
        idx = lines.index("Excel output is below:")
    except ValueError, e:
        print >>sys.stdout, "No Excel output to parse. Aborting."
        sys.exit(1)
    lines = lines[idx + 1:]

    label_flag = 0
    for line in lines:
        line = line.replace("\"", "")

        if label_flag:
            items = [ line ]
            report_name = line
            label_flag = 0
        else:
            items = [ x for x in line.split(" ") if x ]
        if not items:
            label_flag = 1
        if len(items) == 13:
            if (int(items[-1]) - sum([ int(x) for x in items[:-1] ])) == 0x4:
                continue
        if len(items) > 2:
            kilobyte = items[0]
            value = 2
            for item in items[1:]:
                value *= 2
                iozone_data = add_value(iozone_data, item, report_name, kilobyte + "KB", "".join((str(value),"KB record length/sec")), platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    reports = iozone_data.keys()
    for report in reports:
        kb_size = iozone_data[report].keys()
        kb_size = sort_list(kb_size)
        for size in kb_size:
            keys = iozone_data[report][size].keys()
            keys = sort_list(keys)
            template.make_plot(" - ".join((report, size)), iozone_data[report][size], keys, verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/iozone.pickle", iozone_data, platform_sdk)
