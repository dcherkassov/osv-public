#vim: ts=4 expandtab

from benchmark_bonnie import *
from benchmark_customerscreens import *
from benchmark_fio import *
from benchmark_ftest_gdi import *
from benchmark_glmark import *
from benchmark_gtkperf import *
from benchmark_iozone import *
from benchmark_iperf import *
from benchmark_interbench import *
from benchmark_java import *
from benchmark_lmbench2 import *
from benchmark_netperf import *
from benchmark_nbench import *
from benchmark_openssl import *
from benchmark_tiobench import *
from benchmark_unixbench import *
from benchmark_xscreensavers import *

generate_plots = {
    "Benchmark.bonnie": benchmark_bonnie,
    "Benchmark.CustomerScreens": benchmark_customerscreens,
    "Benchmark.fio": benchmark_fio,
    "Benchmark.ftest-gdi": benchmark_ftest_gdi,
    "Benchmark.GLMark": benchmark_glmark,
    "Benchmark.gtkperf": benchmark_gtkperf,
    "Benchmark.IOzone": benchmark_iozone,
    "Benchmark.iperf": benchmark_iperf,
    "Benchmark.Interbench": benchmark_interbench,
    "Benchmark.Java": benchmark_java,
    "Benchmark.lmbench2": benchmark_lmbench2,
    "Benchmark.netperf": benchmark_netperf,
    "Benchmark.nbench-byte": benchmark_nbench,
    "Benchmark.OpenSSL": benchmark_openssl,
    "Benchmark.tiobench": benchmark_tiobench,
    "Benchmark.unixbench": benchmark_unixbench,
    "Benchmark.xscreensavers": benchmark_xscreensavers,
}
