#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.gtkperf"

def parse_value(value):
    value = value.split(":")[-1]
    value = value.strip()
    return value


def benchmark_gtkperf(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ x.strip() for x in f.readlines() if not x.count("gdk_draw_pixbuf") and x.strip() ]

    create_workspace(NAME)
    gtkperf_data = load_data(NAME + "/gtkperf.pickle", platform_sdk)

    for line in lines:
        if line.startswith("GtkPerf"):
            continue
        if line.startswith("Gtk"):
            line = [ i for i in line.split("-") if i ]

            name = "-".join(line[:2]).strip()  + "/millisec"
            name = name.strip()
            if len(line) == 3:
                value = parse_value(line[-1])
                gtkperf_data = add_value(gtkperf_data, value, name, platform_sdk)
        elif line.startswith("time:"):
            value = parse_value(line)
            gtkperf_data = add_value(gtkperf_data, value, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    template.make_plot("GTKPerf Entry Test", gtkperf_data, [ i for i in gtkperf_data.keys() if i.count("Entry") ], verbose = 0)
    template.make_plot("GTKPerf Button Test", gtkperf_data, [ i for i in gtkperf_data.keys() if i.count("Button") ], verbose = 0)
    template.make_plot("GTKPerf GTKTextView Test", gtkperf_data, [ i for i in gtkperf_data.keys() if i.count("TextView") ], verbose = 0)
    template.make_plot("GTKPerf GTKDrawingArea Test", gtkperf_data, [ i for i in gtkperf_data.keys() if i.count("DrawingArea") ], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/gtkperf.pickle", gtkperf_data, platform_sdk)
