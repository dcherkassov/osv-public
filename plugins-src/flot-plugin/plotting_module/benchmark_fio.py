#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.fio"

def parse_value(value):
    value = value.split("=")[-1]
    value = value.replace("KB/s", "")

    return value


def benchmark_fio(filename, platform_sdk = ''):
    f = open(filename, "r")
    test_name = get_test_name(filename)
    lines = [ x.strip() for x in f.readlines() ]

    create_workspace(NAME)
    fio_data = load_data(NAME + "/fio.pickle", platform_sdk)

    group_string = "Run status group"
    group_number = 0
    for line in lines:
        if line.startswith(group_string):
            line = line.replace(group_string, "")
            group_number = int([ i for i in line.split(" ") if i ][0])
        line = line.split(" ")
        name = line[0][:-1]
        if name in [ "READ", "WRITE" ]:
            name = " ".join((name,"KB/s"))
            max_speed = parse_value(line[3])
            fio_data = add_value(fio_data, max_speed, test_name, group_number, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in fio_data.keys():
        for j in fio_data[i].keys():
            template.make_plot("Test %s Group %d" % (i, j), fio_data[i][j], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/fio.pickle", fio_data, platform_sdk)
