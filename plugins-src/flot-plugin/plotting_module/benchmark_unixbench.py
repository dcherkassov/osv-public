#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.unixbench"

def benchmark_unixbench(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ i.strip() for i in f.readlines() if i ]

    if not lines:
       return

    idx = lines.index("-" * 72)
    lines = lines[idx+1:]

    create_workspace(NAME)
    unixbench_data = load_data(NAME + "/unixbench.pickle", platform_sdk)

    for line in lines:
        if line.count("samples"):
            line = [ i.strip() for i in line.split(' ' * 2) if i ]
            name = line[0]
            value = line[1].split(" ")
            name += "/" + value[1]
            value = value[0]

            unixbench_data = add_value(unixbench_data, value, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in unixbench_data.keys():
        short_name = i.split("/")[0]
        template.make_plot("%s Test" % short_name, unixbench_data, i, verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/unixbench.pickle", unixbench_data, platform_sdk)
