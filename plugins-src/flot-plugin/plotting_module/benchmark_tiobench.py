#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.tiobench"

def benchmark_tiobench(filename, platform_sdk = ''):
    f = open(filename, "r")
    test_name = get_test_name(filename)
    lines = [ x.strip() for x in f.readlines() if x ]

    create_workspace(NAME)
    tiobench_data = load_data(NAME + "/tiobench.pickle", platform_sdk)

    for line in lines:
        line = line.split("|")
        line = [ i.strip() for i in line if i ]
        if not line or len(line) == 1:
            continue
        line = clean_line(line)
        name = line[0]
        if name in [ "Item","Total" ]:
            continue
        if not name.count("MBs"): #TODO: Latency graphs
            continue
        value = line[2]
        tiobench_data = add_value(tiobench_data, value, test_name, name, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in tiobench_data.keys():
        for j in [ "Read", "Write" ]:
            template.make_plot("%s %s Test" % (i, j), tiobench_data[i], [ k for k in tiobench_data[i].keys() if k.count(j) ], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/tiobench.pickle", tiobench_data, platform_sdk)
