#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.nbench-byte"

items = [
        'NUMERIC SORT',
        'STRING SORT',
        'BITFIELD',
        'FP EMULATION',
        'FOURIER',
        'ASSIGNMENT',
        'IDEA',
        'HUFFMAN',
        'NEURAL NET',
        'LU DECOMPOSITION',
        'INTEGER INDEX',
        'FLOATING-POINT INDEX',
        'MEMORY INDEX',
        'INTEGER INDEX',
        'FLOATING-POINT INDEX',
    ]


def benchmark_nbench(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ x.strip() for x in f.readlines() if x ]

    create_workspace(NAME)
    nbench_data = load_data(NAME + "/nbench.pickle", platform_sdk)

    for line in lines:
        line = [ i.strip() for i in line.split(":") if i ]
        if not line:
            continue
        test = line[0]
        if test in items:
           value = line[1]
           nbench_data = add_value(nbench_data, value, test, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in nbench_data.keys():
        template.make_plot("%s Test" % capitalize_string(i), nbench_data, i, verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/nbench.pickle", nbench_data, platform_sdk)
