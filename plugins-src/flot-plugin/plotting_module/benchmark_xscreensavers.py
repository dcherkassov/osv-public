#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.xscreensavers"

def get_value(value):
    value = value.split(" ")[-1]
    value = value.replace("%","")
    return eval(value)

def benchmark_xscreensavers(filename, platform_sdk = ''):
    f = open(filename, "r")
    test_name = get_test_name(filename)
    test_name = test_name.capitalize()
    lines = [ x.strip() for x in f.readlines() if x.strip() ]

    if not lines:
        return

    create_workspace(NAME)

    xscreensavers_data = load_data(NAME + "/xscreensavers.pickle", platform_sdk)

    fps = []
    load = []
    for line in lines:
        line = line.split("\t")

        fps.append(get_value(line[0]))
        load.append(get_value(line[1]))

    fps = sum(fps) / len(fps)
    load = sum(load) / len(load)        

    xscreensavers_data = add_value(xscreensavers_data, fps, test_name, "Frames Per Second", platform_sdk)
    xscreensavers_data = add_value(xscreensavers_data, load, test_name, "CPU Load %", platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    keys = xscreensavers_data.keys()
    keys.sort()
    for test in keys:
        template.make_plot(" ".join((test, "Screensaver")), xscreensavers_data[test], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/xscreensavers.pickle", xscreensavers_data, platform_sdk)
