#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.iperf"

def benchmark_iperf(filename, platform_sdk = ''):
    f = open(filename, "r")
    test_name = get_test_name(filename)
    lines = [ x.strip() for x in f.readlines() if x ]

    if not lines:
        return

    create_workspace(NAME)
    iperf_data = load_data(NAME + "/iperf.pickle", platform_sdk)


    if test_name == 'individually':
        lines = [ lines[-1] ]
    if test_name == 'simultaneously':
        lines = lines[-2:]

    for line in lines:
        name = ""
        if line.startswith("[  5]"): #  client to host
            name = "Target to Host/ Mbytes sec"
        if line.startswith("[  4]"): #  host to client
            name = "Host to Target/ Mbytes sec"
        if name:
            value = line.split(" ")[-2]
            iperf_data = add_value(iperf_data, value, test_name, name, platform_sdk)


    template = Template()
    template.set_platform_sdk(platform_sdk)

    for i in iperf_data.keys():
        template.make_plot("%s Test" % i.capitalize(), iperf_data[i], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/iperf.pickle", iperf_data, platform_sdk)
