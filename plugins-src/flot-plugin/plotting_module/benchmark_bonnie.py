#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.bonnie"

def benchmark_bonnie(filename, platform_sdk = ""):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    bonnie_data = load_data(NAME + "/bonnie.pickle", platform_sdk)

    if not lines:
        return
    line = lines[-1]
    line = line.strip()
    items = line.split(",")

    if items == 25:
        return
    array = {
        "4": "Sequential Output (Block)",
        "6": "Sequential Rewrite",
        "10": "Sequential Input (Block)",
        "15": "Sequential Create",
        "19": "Sequential Delete",

        "12": "Random Seeks",
        "21": "Random Create",
        "25": "Random Delete",
    }

    template = Template()
    template.set_platform_sdk(platform_sdk)

    sequential_idx = [ 4, 6, 10, 15, 19 ]
    random_idx = [ 12, 21, 25 ]
    for idx, label in array.items():
        idx = int(idx)
        value = items[idx]
        if value.replace('+',''):
            bonnie_data = add_value(bonnie_data, value, label + "/sec", platform_sdk)
        else:
            if sequential_idx.count(idx): sequential_idx.remove(idx)
            if random_idx.count(idx): random_idx.remove(idx)

    template.make_plot("Bonnie Sequential", bonnie_data, [ array[str(x)] + "/sec" for x in sequential_idx ], verbose = 0) 
    template.make_plot("Bonnie Random", bonnie_data, [ array[str(x)] + "/sec" for x in random_idx ], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/bonnie.pickle", bonnie_data, platform_sdk)
