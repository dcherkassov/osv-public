#vim: ts=4 expandtab

from decorators import *

def index_string(value):
    value.sort()
    value_tuple = []

    j = 1
    for i in value:
        value_tuple.append([j, i[1]])
        j += 1
    return str(value_tuple).replace("'","")

def parse_tick_labels(labels):
    x = 1
    tick_labels = [ [0, ""] ]
    for tick in labels:
        tick_labels.append([x, tick])
        x += 1
    return str(tick_labels)

def parse_json(data):
    output = "\t\t[\n"
    keys = sort_list(data.keys())

    order = 0
    for k in keys:
        lineWidth = ""
        order += 1
        if order == 1:
            lineWidth = "lineWidth: 2"
        output += "\t\t\t{\n"
        output += "\t\t\t\tlabel: \"%s\",\n" % k
        output += "\t\t\t\tdata: %s,\n" % index_string(data[k])
        output += "\t\t\t\tbars: { show: true, barWidth: 0.1, order: %d, %s },\n" % (order, lineWidth)
        output += "\t\t\t},\n"
    output += "\t\t],\n"

    return output

class Template(object):
    def __init__(self):
        self.x = 0
        self.plot = ""
        self.json_array = []
        self.label_array = []
        self.xaxis_ticks = []
        self.template = """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>%PLATFORM_SDK% Plot Results</title>
    <!--[if IE]><script language="javascript" type="text/javascript" src="../excanvas.min.js"></script><![endif]-->
    <script language="javascript" type="text/javascript" src="./flot/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="./flot/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="./flot/jquery.flot.orderBars.js"></script>
    <style>
        .tooltip-bg{
            font-size: 12;
            font-family: verdana, sans-serif;
            background: silver none;
            border: 1px gray solid;
        }
    </style>

 </head>
    <body>
%BODY_DATA%
        <script language="javascript">
        function showTooltip(x, y, contents) {
            $('<div id="tooltip" class="tooltip-bg">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                'z-index':'1010',
                top: y,
                left: x
            }).prependTo("body").show();
        }

        var dataset = [
%DATASET%
        ];
        function displayTooltip (event, pos, item) {
            $("#y").text(pos.y.toFixed(2));

            if (item) {
                y = item.datapoint[1].toFixed(2);
                    
                showTooltip(item.pageX, item.pageY,
                    item.series.label + ": " + y);
            }
            else {
                $("#tooltip").remove();
            }
        }
    
%SCRIPT_DATA%
        </script>
    </body>
</html>
"""

    def set_platform_sdk(self, name):
        self.template = self.template.replace("%PLATFORM_SDK%", name)

    def make_plot(self, label, value, value_list = 0, verbose = 1):
        #label = capitalize_string(label)
        self.label_array.append(label)

        if type(value_list) == str:
            value_list = [ value_list ]
        if not value_list:
            value_list = value.keys() 

        dataset = {}
        for item in value_list:
            self.xaxis_ticks = value[item].keys() 
            for i in self.xaxis_ticks:
                if not value.has_key(item):
                    continue
                j = value[item][i]["value"]
                full_label = " ".join([ i[0].upper() + i[1:] for i in item.split(" ")])
                if verbose:
                    full_label = " ".join((label, full_label))
                if not dataset.has_key(full_label):
                    dataset[full_label] = []
                dataset[full_label].append((i,j))
        self.json_array.append(parse_json(dataset))
        self.x += 1

    def populate_plots(self):
        body_data = ""
        for x in range(0, self.x):
            body_data += """\t<table align=center>\n"""
            body_data += """\t\t<tr><td align=center><h2> %s Results </h2></td></tr>\n""" % self.label_array[x]
            body_data += """\t\t<tr><td><div id="placeholder_%d" style="width:600px;height:300px;"></div></td></tr>\n""" % x
            body_data += """\t</table>\n\n"""

        self.template = self.template.replace("%BODY_DATA%", body_data)
        self.template = self.template.replace("%DATASET%","".join(self.json_array))

        script_data = ""
        for x in range(0, self.x):
            script_data += """\t$.plot($("#placeholder_%d"), dataset[%d], {
            xaxis: { min: 0, ticks: %s },
            grid: { hoverable: true, clickable: true }, })\n""" % (x, x, parse_tick_labels(self.xaxis_ticks))

            script_data += """\t<!-- $("#placeholder_%d").bind("plothover", function (event, pos, item) { displayTooltip(event, pos, item); }); -->\n""" % x
        self.template = self.template.replace("%SCRIPT_DATA%", script_data)

    def write_file(self, filename):
        self.populate_plots()

        f = open(filename, "w")
        f.write(self.template)
        f.close()
