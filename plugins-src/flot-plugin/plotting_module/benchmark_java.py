#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.Java"

def find_prefix(value):
    j = 1
    for i in value[1:]:
        if i.isupper():
            break
        j += 1

    return value[:j]

def benchmark_java(filename, platform_sdk = ''):
    f = open(filename, "r")
    lines = [ i.strip() for i in f.readlines() if i.strip().endswith("ms") ]

    create_workspace(NAME)
    java_data = load_data(NAME + "/java.pickle", platform_sdk)

    for line in lines:
        line = line.replace(":", "")
        items = [ i for i in line.split(" ") if i ]

        if items[0] == 'Totals':
            continue

        value = items[2].replace("ms", "")
        java_data = add_value(java_data, value, find_prefix(items[0]), items[0] + "/millsecs", platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)
    for section in java_data.keys():
        template.make_plot(section, java_data[section], verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/java.pickle", java_data, platform_sdk)
