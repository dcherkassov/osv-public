#vim: ts=4 expandtab

from template import *
from decorators import *

NAME = "Benchmark.CustomerScreens"

def benchmark_customerscreens(filename, platform_sdk = ""):
    f = open(filename, "r")
    lines = f.readlines()

    create_workspace(NAME)
    customerscreens_data = load_data(NAME + "/customerscreens.pickle", platform_sdk)

    screens = []
    for line in lines:
        line = line.replace("\t","")

        if line.startswith("<testname>"):
            screen = line[10:-12]
            screens.append(screen)
        else:
            value = line[12:-14]

        if line.startswith("<switchtime>"):
            customerscreens_data = add_value(customerscreens_data, value, "switch time", screen, platform_sdk)

        if line.startswith("<avgcputime>"):
            customerscreens_data = add_value(customerscreens_data, value, "avg cpu usage", screen, platform_sdk)

        if line.startswith("<maxcputime>"):
            customerscreens_data = add_value(customerscreens_data, value, "max cpu usage", screen, platform_sdk)

    template = Template()
    template.set_platform_sdk(platform_sdk)

    template.make_plot("Screen Switch Time", customerscreens_data['switch time'], screens, verbose = 0)
    template.make_plot("Screen Average CPU Usage", customerscreens_data['avg cpu usage'], screens, verbose = 0)
    template.make_plot("Screen Max CPU Usage", customerscreens_data['max cpu usage'], screens, verbose = 0)

    template.write_file(NAME + "/reports/index.html")
    save_data(NAME + "/customerscreens.pickle", customerscreens_data, platform_sdk)
